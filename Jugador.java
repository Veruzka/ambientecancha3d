
package vmorales;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glRotatef;
import static org.lwjgl.opengl.GL11.glTexCoord2d;
import static org.lwjgl.opengl.GL11.glTranslatef;
import static org.lwjgl.opengl.GL11.glVertex3f;


public class Jugador {
    
    
    
    public void drawCabeza (float x, float y, float z){
        
        glBegin (GL_QUADS);        
        
        glTexCoord2d(0,0);glVertex3f (-16f-x,0.6f-y,8.35f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-16f-x,0.6f-y,8.65f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-16f-x,0.85f-y,8.65f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-16f-x,0.85f-y,8.35f-z);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-16f-x,0.85f-y,8.35f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-16f-x,0.85f-y,8.65f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.75f-x,0.85f-y,8.65f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.75f-x,0.85f-y,8.35f-z);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-16f-x,0.6f-y,8.35f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.75f-x,0.6f-y,8.35f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.75f-x,0.85f-y,8.35f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-16f-x,0.85f-y,8.35f-z);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-16f-x,0.6f-y,8.65f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.75f-x,0.6f-y,8.65f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.75f-x,0.85f-y,8.65f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-16f-x,0.85f-y,8.65f-z);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-15.75f-x,0.6f-y,8.35f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.75f-x,0.6f-y,8.65f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.75f-x,0.85f-y,8.65f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.75f-x,0.85f-y,8.35f-z);//esquina superior izquierda 
        
        glEnd();
        
    }
    

    public void drawCamisa(float x, float y, float z){
        
        glBegin (GL_QUADS);        
        
        glTexCoord2d(0,0);glVertex3f (-15.9f-x,0.2f-y,8.35f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.9f-x,0.2f-y,8.65f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.9f-x,0.6f-y,8.65f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.9f-x,0.6f-y,8.35f-z);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-15.8f-x,0.2f-y,8.35f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.8f-x,0.2f-y,8.65f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.8f-x,0.6f-y,8.65f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.8f-x,0.6f-y,8.35f-z);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-15.9f-x,0.2f-y,8.35f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.8f-x,0.2f-y,8.35f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.8f-x,0.6f-y,8.35f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.9f-x,0.6f-y,8.35f-z);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-15.9f-x,0.2f-y,8.65f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.8f-x,0.2f-y,8.65f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.8f-x,0.6f-y,8.65f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.9f-x,0.6f-y,8.65f-z);//esquina superior izquierda 
        
        glEnd();
        
    }
    
    public void drawNumero(float x, float y, float z){
        
        glBegin (GL_QUADS);        
        
        glTexCoord2d(0,0);glVertex3f (-15.905f-x,0.25f-y,8.40f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.905f-x,0.25f-y,8.60f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.905f-x,0.45f-y,8.60f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.905f-x,0.45f-y,8.40f-z);//esquina superior izquierda 
        
        glEnd();
        
    }
    
    public void drawMangas (float x, float y, float z){
        
        glBegin (GL_QUADS);        
        
        glTexCoord2d(0,0);glVertex3f (-15.9f-x,0.5f-y,8.65f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.9f-x,0.5f-y,8.72f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.9f-x,0.6f-y,8.72f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.9f-x,0.6f-y,8.65f-z);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-15.8f-x,0.5f-y,8.28f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.8f-x,0.5f-y,8.35f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.8f-x,0.6f-y,8.35f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.8f-x,0.6f-y,8.28f-z);//esquina superior izquierda 
       
        glTexCoord2d(0,0);glVertex3f (-15.9f-x,0.5f-y,8.72f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.8f-x,0.5f-y,8.72f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.8f-x,0.6f-y,8.72f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.9f-x,0.6f-y,8.72f-z);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-15.9f-x,0.5f-y,8.28f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.8f-x,0.5f-y,8.28f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.8f-x,0.6f-y,8.28f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.9f-x,0.6f-y,8.28f-z);//esquina superior izquierda 
        
        glEnd();
    }
    
    public void drawBrazos (float x, float y, float z){
        
        glBegin (GL_QUADS);        
        
        glTexCoord2d(0,0);glVertex3f (-15.9f-x,0.2f-y,8.65f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.9f-x,0.2f-y,8.72f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.9f-x,0.5f-y,8.72f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.9f-x,0.5f-y,8.65f-z);//esquina superior izquierda 
       
        glTexCoord2d(0,0);glVertex3f (-15.9f-x,0.2f-y,8.28f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.9f-x,0.2f-y,8.35f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.9f-x,0.5f-y,8.35f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.9f-x,0.5f-y,8.28f-z);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-15.8f-x,0.2f-y,8.28f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.8f-x,0.2f-y,8.35f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.8f-x,0.5f-y,8.35f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.8f-x,0.5f-y,8.28f-z);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-15.9f-x,0.2f-y,8.72f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.8f-x,0.2f-y,8.72f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.8f-x,0.5f-y,8.72f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.9f-x,0.5f-y,8.72f-z);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-15.9f-x,0.2f-y,8.28f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.8f-x,0.2f-y,8.28f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.8f-x,0.5f-y,8.28f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.9f-x,0.5f-y,8.28f-z);//esquina superior izquierda 
        
        glEnd();
    }
    public void drawPantaloneta (float x, float y, float z){
        
        glBegin (GL_QUADS);        
        
        glTexCoord2d(0,0);glVertex3f (-15.9f-x,0.05f-y,8.35f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.9f-x,0.05f-y,8.65f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.9f-x,0.2f-y,8.65f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.9f-x,0.2f-y,8.35f-z);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-15.8f-x,0.05f-y,8.35f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.8f-x,0.05f-y,8.65f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.8f-x,0.2f-y,8.65f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.8f-x,0.2f-y,8.35f-z);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-15.9f-x,0.05f-y,8.35f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.8f-x,0.05f-y,8.35f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.8f-x,0.2f-y,8.35f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.9f-x,0.2f-y,8.35f-z);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-15.9f-x,0.05f-y,8.65f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.8f-x,0.05f-y,8.65f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.8f-x,0.2f-y,8.65f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.9f-x,0.2f-y,8.65f-z);//esquina superior izquierda 
        
        glEnd();
    }
    
    public void drawPiernas(float x, float y, float z){
        
        glBegin (GL_QUADS);        
        
        glTexCoord2d(0,0);glVertex3f (-15.9f-x,-0.4f-y,8.52f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.9f-x,-0.4f-y,8.62f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.9f-x,0.05f-y,8.62f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.9f-x,0.05f-y,8.52f-z);//esquina superior izquierda 
       
        glTexCoord2d(0,0);glVertex3f (-15.8f-x,-0.4f-y,8.52f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.8f-x,-0.4f-y,8.62f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.8f-x,0.05f-y,8.62f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.8f-x,0.05f-y,8.52f-z);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-15.9f-x,-0.4f-y,8.37f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.9f-x,-0.4f-y,8.47f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.9f-x,0.05f-y,8.47f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.9f-x,0.05f-y,8.37f-z);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-15.8f-x,-0.4f-y,8.37f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.8f-x,-0.4f-y,8.47f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.8f-x,0.05f-y,8.47f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.8f-x,0.05f-y,8.37f-z);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-15.9f-x,-0.4f-y,8.37f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.8f-x,-0.4f-y,8.37f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.8f-x,0.05f-y,8.37f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.9f-x,0.05f-y,8.37f-z);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-15.9f-x,-0.4f-y,8.62f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.8f-x,-0.4f-y,8.62f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.8f-x,0.05f-y,8.62f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.9f-x,0.05f-y,8.62f-z);//esquina superior izquierda 
        
        glEnd();
    }
    
    public void drawZapatos(float x, float y, float z){
        
        glBegin (GL_QUADS);        
        
        glTexCoord2d(0,0);glVertex3f (-15.9f-x,-0.5f-y,8.52f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.9f-x,-0.5f-y,8.62f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.9f-x,-0.4f-y,8.62f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.9f-x,-0.4f-y,8.52f-z);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-15.85f-x,-0.5f-y,8.52f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.85f-x,-0.5f-y,8.62f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.85f-x,-0.4f-y,8.62f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.85f-x,-0.4f-y,8.52f-z);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-15.9f-x,-0.5f-y,8.37f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.9f-x,-0.5f-y,8.47f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.9f-x,-0.4f-y,8.47f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.9f-x,-0.4f-y,8.37f-z);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-15.85f-x,-0.5f-y,8.37f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.85f-x,-0.5f-y,8.47f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.85f-x,-0.4f-y,8.47f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.85f-x,-0.4f-y,8.37f-z);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-15.9f-x,-0.5f-y,8.37f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.85f-x,-0.5f-y,8.37f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.85f-x,-0.4f-y,8.37f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.9f-x,-0.4f-y,8.37f-z);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-15.9f-x,-0.5f-y,8.62f-z);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-15.85f-x,-0.5f-y,8.62f-z);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-15.85f-x,-0.4f-y,8.62f-z);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-15.9f-x,-0.4f-y,8.62f-z);//esquina superior izquierda 
        
        glEnd();
    }

    
}
