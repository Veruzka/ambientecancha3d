package vmorales;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.util.glu.GLU.*;
import org.lwjgl.util.glu.Sphere;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

// ME QUEDO EN LOS VESTIDORES 
public class Main {

    public static Camara cam;
    public static float TrasladaZ = 0, x = 0, y = 0, z = 0, c = 0, o = 0;
    public static boolean flag = false;

    public static void main(String[] args) {
        initDisplay();
        initGL();

        gameLoop();
        cleanUp();

    }

    public static void initGL() {

        cam = new Camara(70, (float) Display.getWidth() / (float) Display.getHeight(), 0.8f, 500);

        glClearColor(0, 0, 0, 1);

        glEnable(GL_DEPTH_TEST);
        //glEnable(GL_CULL_FACE); //para ocultar caras del cubo
        glEnable(GL_COLOR_MATERIAL);
        glEnable(GL_TEXTURE_2D);
    }

    public static void gameLoop() {

        Escenarios esc = new Escenarios();
        Paredes pared = new Paredes();
        Techo techo = new Techo();
        Pisos piso = new Pisos();

        Puertas puerta1 = new Puertas();
        Vestidor vest = new Vestidor();
        Duchas ducha = new Duchas();
        Baño baño = new Baño();
        Rehabilitacion reha = new Rehabilitacion();
        Tribuna trib = new Tribuna();
        Jugador J = new Jugador();
        Cielo cielo = new Cielo();
        Arboles arbol = new Arboles();
        Tactica tac = new Tactica();
        String[][] matriz = esc.escenarioUno();

        //TEXTURAS (imagenes 512x512)
        Texture tPared = loadTexture("paredestasi");
        Texture tTecho = loadTexture("pared11");
        Texture tTechoext = loadTexture("techo2");
        Texture tParedbaño = loadTexture("paredbaño1");
        Texture tPiso = loadTexture("pisomadera");
        Texture tPisoBaño = loadTexture("pisobaño");
        Texture tPisoTerapia = loadTexture("pisoterapia");
        Texture tPisoExterior = loadTexture("pisocafe");
        Texture tCancha = loadTexture("cesped");
        Texture tExtCancha = loadTexture("paredpiedrapiedra");
        Texture tPuerta = loadTexture("puerta3");
        Texture tVest = loadTexture("madera1");
        Texture tVest1 = loadTexture("madera3");
        Texture tVestasiento = loadTexture("cuero1");
        Texture tDucha = loadTexture("metal");
        Texture tTechoTrib = loadTexture("techotrib");
        Texture tGraderio = loadTexture("cemento1");
        Texture tVigas = loadTexture("cemento2");
        Texture tPintLineas = loadTexture("pinturablanca");
        Texture tCinta = loadTexture("caucho");
        Texture tCaminadora = loadTexture("caminadora");
        Texture tCubiculos = loadTexture("bañometal");
        Texture tCubPuerta = loadTexture("madera3");
        Texture tMezon = loadTexture("meson");
        Texture tEspejo = loadTexture("espejo");
        Texture tBaño = loadTexture("marmol");
        Texture tFondoBaño = loadTexture("marmol2");
        Texture tPelo = loadTexture("pelo");
        Texture tCamisa = loadTexture("camiseta");
        Texture tNumero = loadTexture("numero");
        Texture tPiel = loadTexture("piel");
        Texture tPantaloneta = loadTexture("pantaloneta");
        Texture tZapatos = loadTexture("zapatos");
        Texture tColchoneta = loadTexture("colchoneta");
        Texture tConos = loadTexture("cono");
        Texture tCielo = loadTexture("cielo");
        Texture tTierra = loadTexture("piedra2");
        Texture tTronco = loadTexture("tronco");
        Texture tArbol1 = loadTexture("arbol1");
        Texture tArbol2 = loadTexture("arbol2");
        Texture tBase = loadTexture("tierra");
        Texture tBancas = loadTexture("bancas1");
        Texture tTactica = loadTexture("tactica1");
        Texture tArcos = loadTexture("arco");

        while (!Display.isCloseRequested()) {
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glShadeModel(GL_SMOOTH);
            glLoadIdentity();

            cam.useView();

            //CREAR PISOS
            //piso exterior
            tPisoExterior.bind();
            piso.drawPisoExterior(1);
            //piso baño y duchas
            tPisoBaño.bind();
            piso.drawPisoBaño(1);
            //piso terapia
            tPisoTerapia.bind();
            piso.drawPisoTerapia(1);
            //Cancha
            tCancha.bind();
            piso.drawCancha(1);
            //piso interior
            tPiso.bind();
            piso.drawPiso(1);
            //Piso exteriores cancha
            tExtCancha.bind();
            piso.drawExtCancha(1);

            //Lineas cancha
            tPintLineas.bind();
            piso.drawLineas();

            //CREAR TECHOS
            //Techo interno
            tTecho.bind();
            techo.drawTecho(1);
            //Techo externo
            tTechoext.bind();
            techo.drawTechoext();
            //Techo tribuna
            tTechoTrib.bind();
            techo.drawTechoTribuna();

            //CREAR PUERTAS
            //puertas
            tPuerta.bind();
            puerta1.drawPuertaPrincipal(1, TrasladaZ);

            //CREAR VESTIDOR 
            //Vestidores
            tVest.bind();
            vest.drawVestidores(1);

            tVest1.bind();
            vest.vestseparaciones();

            tVestasiento.bind();
            vest.vestAsientos();

            tCamisa.bind();
            vest.drawCamisas();

            //CREAR DUCHAS 
            //Jacuzzi
            tParedbaño.bind();
            ducha.drawJacu();
            // duchas
            tDucha.bind();
            ducha.drawDuchas();

            //CREAR SALON REHABILITACIÓN
            tCaminadora.bind();
            reha.drawCaminadoras(1);

            tCinta.bind();
            reha.drawCinta();

            tCinta.bind();
            reha.drawCir();

            tCinta.bind();
            reha.drawCir1();

            tCinta.bind();
            reha.drawCir2();

            tColchoneta.bind();
            reha.drawColchonetas();

            tConos.bind();
            reha.drawConos();

            glPushMatrix();
            tVigas.bind();
            reha.drawPelota(c, o);
            glPopMatrix();

            tBancas.bind();
            reha.drawBancas();

            //CREAR SALON TACTICA
            tTactica.bind();
            tac.drawPizarra();

            //CREAR TRIBUNA
            tGraderio.bind();
            trib.drawGradas();

            tVigas.bind();
            trib.drawVigas();

            tArcos.bind();
            trib.drawArcos();

            //CREAR BAÑO
            tCubiculos.bind();
            baño.drawCubiculos();

            tCubPuerta.bind();
            baño.drawPuertasBaño();

            tMezon.bind();
            baño.drawMezon();

            tBaño.bind();
            baño.drawBaños();

            tFondoBaño.bind();
            baño.drawFondoBaño();

            tEspejo.bind();
            baño.drawEspejo();

            //CREAR JUGADOR
            glPushMatrix();
//            glRotatef(a, 0, 1, 0);

            tPelo.bind();
            J.drawCabeza(x, y, z);

            tCamisa.bind();
            J.drawCamisa(x, y, z);

            tCamisa.bind();
            J.drawMangas(x, y, z);

            tPiel.bind();
            J.drawBrazos(x, y, z);

            tPantaloneta.bind();
            J.drawPantaloneta(x, y, z);

            tPiel.bind();
            J.drawPiernas(x, y, z);

            tZapatos.bind();
            J.drawZapatos(x, y, z);

            tNumero.bind();
            J.drawNumero(x, y, z);

            glPopMatrix();

            //CREAR CIELO
            tCielo.bind();
            cielo.drawCielo();

            tTierra.bind();
            cielo.drawTierra();

            //CREAR ARBOLES
            tArbol1.bind();
            arbol.drawArbol1();

            tArbol2.bind();
            arbol.drawArbol2();

            tBase.bind();
            arbol.drawBase();

            tTronco.bind();
            arbol.drawTroncos();

            //CREAR PAREDES
            for (int x = 0; x < matriz.length; x++) {
                for (int z = 0; z < matriz[x].length; z++) {
                    glTranslatef(x, 0, z);

                    //DIBUJA PAREDES EXTERNAS
                    tPared.bind();
                    if (matriz[x][z].equals("f")) {
                        tPared.bind();
                        pared.drawParedFrontal(1);
                    }
                    if (matriz[x][z].equals("l")) {
                        pared.drawParedLateral(1);
                    }

                    if (matriz[x][z].equals("lbf")) {
                        pared.drawParedLateralBordeF(1);
                    }
                    if (matriz[x][z].equals("lbp")) {
                        pared.drawParedLateralBordeP(1);
                    }
                    if (matriz[x][z].equals("fbi")) {
                        pared.drawParedFrontalBordeI(1);
                    }
                    if (matriz[x][z].equals("fbd")) {
                        pared.drawParedFrontalBordeD(1);
                    }
                    //DIBUJA PAREDES INTERNAS
                    //paredes baño
                    tParedbaño.bind();
                    if (matriz[x][z].equals("pfb")) {
                        pared.drawParedFrontalBaño(1);
                    }
                    if (matriz[x][z].equals("plb")) {
                        pared.drawParedLateralBaño(1);
                    }
                    if (matriz[x][z].equals("lb1b")) {
                        pared.drawParedLateralBorde1Baño(1);
                    }
                    if (matriz[x][z].equals("lb2b")) {
                        pared.drawParedLateralBorde2Baño(1);
                    }
                    if (matriz[x][z].equals("fbib")) {
                        pared.drawParedFrontalBordeIBaño(1);
                    }
                    if (matriz[x][z].equals("lbpb")) {
                        pared.drawParedLateralBordePBaño(1);
                    }

                    //paredes del resto del complejo
                    tPared.bind();
                    if (matriz[x][z].equals("lb1")) {
                        pared.drawParedLateralBorde1(1);
                    }
                    if (matriz[x][z].equals("lb2")) {
                        pared.drawParedLateralBorde2(1);
                    }
                    if (matriz[x][z].equals("fb1")) {
                        pared.drawParedFrontalBorde1(1);
                    }

                    glTranslatef(-x, 0, -z);

                }
            }

            controles();
            Display.update();

        }
    }
//Método para moverse usando teclas y mouse

    public static void controles() {
        //por teclado
        boolean forward = Keyboard.isKeyDown(Keyboard.KEY_UP);
        boolean backward = Keyboard.isKeyDown(Keyboard.KEY_DOWN);
        boolean left = Keyboard.isKeyDown(Keyboard.KEY_LEFT);
        boolean right = Keyboard.isKeyDown(Keyboard.KEY_RIGHT);
        boolean forwardJ = Keyboard.isKeyDown(Keyboard.KEY_G);
        boolean backwardJ = Keyboard.isKeyDown(Keyboard.KEY_B);
        boolean leftJ = Keyboard.isKeyDown(Keyboard.KEY_V);
        boolean rightJ = Keyboard.isKeyDown(Keyboard.KEY_N);
        boolean subir = Keyboard.isKeyDown(Keyboard.KEY_W);
        boolean bajar = Keyboard.isKeyDown(Keyboard.KEY_S);
        boolean abrirPuerta = Keyboard.isKeyDown(Keyboard.KEY_A);
        boolean cerrarPuerta = Keyboard.isKeyDown(Keyboard.KEY_C);
        boolean vistaGeneral = Keyboard.isKeyDown(Keyboard.KEY_1);
        boolean vistaCancha = Keyboard.isKeyDown(Keyboard.KEY_2);
        boolean vistaPrincipal = Keyboard.isKeyDown(Keyboard.KEY_4);
        boolean vistaJugador = Keyboard.isKeyDown(Keyboard.KEY_3);
        boolean elipse = Keyboard.isKeyDown(Keyboard.KEY_E);
        boolean vistaObjeto = Keyboard.isKeyDown(Keyboard.KEY_5);
        boolean obCrece = Keyboard.isKeyDown(Keyboard.KEY_K);
        boolean obDecrece = Keyboard.isKeyDown(Keyboard.KEY_L);

        //mover por teclado CAMARA NORMAL
        if (forward) {
            cam.moveZ(0.04f);
        }
        if (backward) {
            cam.moveZ(-0.04f);
        }
        if (left) {
            cam.moveX(0.04f);
        }
        if (right) {
            cam.moveX(-0.04f);
        }
        if (subir) {
            cam.moveY(-0.04f);
        }
        if (bajar) {
            cam.moveY(0.04f);
        }

        //mover por teclado CAMARA JUGADOR
        if (forwardJ) {
            cam.moveZ(0.04f);
            x = x - 0.04f;
        }
        if (backwardJ) {
            cam.moveZ(-0.04f);
            x = x + 0.04f;
        }
        if (leftJ) {
            cam.moveX(0.04f);
            z = z + 0.04f;
        }
        if (rightJ) {
            cam.moveX(-0.04f);
            z = z - 0.04f;
        }

        //PUERTA
        if (abrirPuerta) {
            TrasladaZ = TrasladaZ + 0.01f;
        }
        if (cerrarPuerta) {
            TrasladaZ = TrasladaZ - 0.01f;
        }

        //CAMARA ELIPSE
        if (elipse) {
            cam.moveX(-0.1f);
            cam.rotateY(-0.2f);
        }

        //VISTAS CAMARA
        if (vistaGeneral) {
            cam.vistaGeneral();
        }
        if (vistaCancha) {
            cam.vistaCancha();
        }
        if (vistaJugador) {
            cam.vistaJugador();

        }
        if (vistaPrincipal) {
            cam.vistaPrincipal();
        }

        //para cambiar de tamaño al objeto
        if (obCrece) {
            o = o + 0.01f;
        }
        if (obDecrece) {
            o = o - 0.01f;
        }

        //camara que sigue objeto
        if (vistaObjeto) {
            cam.vistaObjeto();
            flag = true;

        }

        if (flag) {
            for (int i = 0; i < 5; i++) {

                if (c <= 2) {
                    cam.moveX(0.0004f);
                    c = c + 0.0004f;
                }
            }
        }

//        if (mueveODer) {
//            if (c >= 0.4) {
//                cam.moveX(-0.04f);
//                c = c - 0.04f;
//            }
//        }
        //por mouse ROTACION CAMARA
        float mx = Mouse.getDX();
        float my = Mouse.getDY();
        mx *= 0.10f;
        my *= 0.10f;

        //mover por mouse
        //se pone 0 para el click izquierdo y 1 para el derecho
        if (Mouse.isButtonDown(0)) {
            cam.rotateY(mx);
            cam.rotateX(-my);
//            b=mx;
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
            cleanUp();
        }
    }

    public static void cleanUp() {
        Display.destroy();
        System.exit(0);
    }

    public static void initDisplay() {
        try {
            Display.setDisplayMode(new DisplayMode(800, 600));
            Display.setTitle("Lienzo. Lwjgl: " + Sys.getVersion());
            Display.create();
        } catch (LWJGLException ex) {
            ex.printStackTrace();
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(0);
        }
    }

    public static Texture loadTexture(String key) {
        try {
            return TextureLoader.getTexture("jpg", new FileInputStream(
                    new File("res/" + key + ".jpg")));

        } catch (IOException ex) {
            Logger.getLogger(Main.class
                    .getName()).log(
                            Level.SEVERE, null, ex);
        }
        return null;
    }

    public static FloatBuffer asFloatBuffer(float[] arreglo) {
        FloatBuffer fb = BufferUtils.createFloatBuffer(arreglo.length);
        fb.put(arreglo);
        fb.flip();
        return fb;
    }

    public static IntBuffer asIntBuffer(int[] arreglo) {
        IntBuffer ib = BufferUtils.createIntBuffer(arreglo.length);
        ib.put(arreglo);
        ib.flip();
        return ib;
    }
}
