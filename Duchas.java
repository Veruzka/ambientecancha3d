/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vmorales;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static org.lwjgl.opengl.GL11.GL_LINES;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glColor3f;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glTexCoord2d;
import static org.lwjgl.opengl.GL11.glVertex3f;

/**
 *
 * @author abonarte musica
 */
public class Duchas {
    double Pi=3.14;
    public void drawJacu (){
        
        glBegin (GL_QUADS);  
        
        //Jacuzzi
        glTexCoord2d(0,0);glVertex3f (15f,0.05f,3.5f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (15f,0.05f,0.35f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (15f,-0.5f,0.35f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (15f,-0.5f,3.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (13.1f,0.05f,3.5f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (13.1f,0.05f,0.35f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (13.1f,-0.5f,0.35f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (13.1f,-0.5f,3.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (14.9f,0.05f,3.5f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (14.9f,0.05f,0.35f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (14.9f,-0.5f,0.35f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (14.9f,-0.5f,3.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (13.1f,0.05f,3.5f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (13.1f,0.05f,0.35f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (13f,0.05f,0.35f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (13f,0.05f,3.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (15f,-0.5f,3.5f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (13f,-0.5f,3.5f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (13f,0.05f,3.5f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (15f,0.05f,3.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (15f,-0.5f,0.36f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (13f,-0.5f,0.36f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (13f,0.05f,0.36f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (15f,0.05f,0.36f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (15f,-0.5f,3.4f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (13f,-0.5f,3.4f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (13f,0.05f,3.4f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (15f,0.05f,3.4f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (14.9f,0.05f,3.5f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (14.9f,0.05f,0.35f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (15f,0.05f,0.35f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (15f,0.05f,3.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (15f,0.05f,3.5f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (15f,0.05f,3.4f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (13f,0.05f,3.4f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (13f,0.05f,3.5f);//esquina superior izquierda
        
        glEnd();
        
    }
    
    public void drawDuchas () {
        
        glBegin (GL_QUADS); 
        
        //DIBUJA DUCHAS
        
        //ducha1
        glTexCoord2d(0,0);glVertex3f (15.7f,1.2f,0.305f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (15.7f,1.3f,0.305f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (15.6f,1.3f,0.305f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (15.6f,1.2f,0.305f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (15.68f,1.2f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (15.68f,1.26f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (15.62f,1.26f,0.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (15.62f,1.2f,0.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (15.7f,1.2f,0.305f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (15.68f,1.2f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (15.62f,1.2f,0.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (15.6f,1.2f,0.305f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (15.7f,1.3f,0.305f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (15.68f,1.26f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (15.62f,1.26f,0.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (15.6f,1.3f,0.305f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (15.7f,1.2f,0.305f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (15.68f,1.2f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (15.68f,1.26f,0.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (15.7f,1.3f,0.305f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (15.6f,1.2f,0.305f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (15.62f,1.2f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (15.62f,1.26f,0.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (15.6f,1.3f,0.305f);//esquina superior izquierda

        //ducha2
        glTexCoord2d(0,0);glVertex3f (16.7f,1.2f,0.305f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (16.7f,1.3f,0.305f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (16.6f,1.3f,0.305f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (16.6f,1.2f,0.305f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (16.68f,1.2f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (16.68f,1.26f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (16.62f,1.26f,0.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (16.62f,1.2f,0.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (16.7f,1.2f,0.305f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (16.68f,1.2f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (16.62f,1.2f,0.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (16.6f,1.2f,0.305f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (16.7f,1.3f,0.305f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (16.68f,1.26f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (16.62f,1.26f,0.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (16.6f,1.3f,0.305f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (16.7f,1.2f,0.305f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (16.68f,1.2f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (16.68f,1.26f,0.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (16.7f,1.3f,0.305f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (16.6f,1.2f,0.305f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (16.62f,1.2f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (16.62f,1.26f,0.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (16.6f,1.3f,0.305f);//esquina superior izquierda
        
        //ducha3
        glTexCoord2d(0,0);glVertex3f (17.7f,1.2f,0.305f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (17.7f,1.3f,0.305f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (17.6f,1.3f,0.305f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (17.6f,1.2f,0.305f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (17.68f,1.2f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (17.68f,1.26f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (17.62f,1.26f,0.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (17.62f,1.2f,0.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (17.7f,1.2f,0.305f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (17.68f,1.2f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (17.62f,1.2f,0.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (17.6f,1.2f,0.305f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (17.7f,1.3f,0.305f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (17.68f,1.26f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (17.62f,1.26f,0.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (17.6f,1.3f,0.305f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (17.7f,1.2f,0.305f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (17.68f,1.2f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (17.68f,1.26f,0.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (17.7f,1.3f,0.305f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (17.6f,1.2f,0.305f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (17.62f,1.2f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (17.62f,1.26f,0.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (17.6f,1.3f,0.305f);//esquina superior izquierda
        
        
        //ducha4
        glTexCoord2d(0,0);glVertex3f (18.65f,1.2f,1.4f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.65f,1.3f,1.4f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.65f,1.3f,1.3f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.65f,1.2f,1.3f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.36f,1.2f,1.38f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.36f,1.26f,1.38f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.36f,1.26f,1.32f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.36f,1.2f,1.32f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.65f,1.2f,1.4f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.36f,1.2f,1.38f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.36f,1.2f,1.32f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.65f,1.2f,1.3f);//esquina superior izquierda
     
        glTexCoord2d(0,0);glVertex3f (18.65f,1.3f,1.4f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.36f,1.26f,1.38f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.36f,1.26f,1.32f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.65f,1.3f,1.3f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.65f,1.2f,1.4f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.36f,1.2f,1.38f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.36f,1.26f,1.38f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.65f,1.3f,1.4f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.65f,1.2f,1.4f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.36f,1.2f,1.32f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.36f,1.26f,1.32f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.65f,1.3f,1.4f);//esquina superior izquierda
        
        //ducha5
        glTexCoord2d(0,0);glVertex3f (18.65f,1.2f,2.4f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.65f,1.3f,2.4f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.65f,1.3f,2.3f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.65f,1.2f,2.3f);//esquina superior izquierda
       
        glTexCoord2d(0,0);glVertex3f (18.36f,1.2f,2.38f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.36f,1.26f,2.38f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.36f,1.26f,2.32f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.36f,1.2f,2.32f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.65f,1.2f,2.4f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.36f,1.2f,2.38f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.36f,1.2f,2.32f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.65f,1.2f,2.3f);//esquina superior izquierda
     
        glTexCoord2d(0,0);glVertex3f (18.65f,1.3f,2.4f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.36f,1.26f,2.38f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.36f,1.26f,2.32f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.65f,1.3f,2.3f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.65f,1.2f,2.4f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.36f,1.2f,2.38f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.36f,1.26f,2.38f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.65f,1.3f,2.4f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.65f,1.2f,2.4f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.36f,1.2f,2.32f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.36f,1.26f,2.32f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.65f,1.3f,2.4f);//esquina superior izquierda
        
        //ducha6
        glTexCoord2d(0,0);glVertex3f (18.65f,1.2f,3.4f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.65f,1.3f,3.4f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.65f,1.3f,3.3f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.65f,1.2f,3.3f);//esquina superior izquierda
       
        glTexCoord2d(0,0);glVertex3f (18.36f,1.2f,3.38f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.36f,1.26f,3.38f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.36f,1.26f,3.32f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.36f,1.2f,3.32f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.65f,1.2f,3.4f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.36f,1.2f,3.38f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.36f,1.2f,3.32f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.65f,1.2f,3.3f);//esquina superior izquierda
     
        glTexCoord2d(0,0);glVertex3f (18.65f,1.3f,3.4f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.36f,1.26f,3.38f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.36f,1.26f,3.32f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.65f,1.3f,3.3f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.65f,1.2f,3.4f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.36f,1.2f,3.38f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.36f,1.26f,3.38f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.65f,1.3f,3.4f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.65f,1.2f,3.4f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.36f,1.2f,3.32f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.36f,1.26f,3.32f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.65f,1.3f,3.4f);//esquina superior izquierda
        
         //ducha7
        glTexCoord2d(0,0);glVertex3f (18.65f,1.2f,4.4f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.65f,1.3f,4.4f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.65f,1.3f,4.3f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.65f,1.2f,4.3f);//esquina superior izquierda
       
        glTexCoord2d(0,0);glVertex3f (18.36f,1.2f,4.38f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.36f,1.26f,4.38f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.36f,1.26f,4.32f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.36f,1.2f,4.32f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.65f,1.2f,4.4f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.36f,1.2f,4.38f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.36f,1.2f,4.32f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.65f,1.2f,4.3f);//esquina superior izquierda
     
        glTexCoord2d(0,0);glVertex3f (18.65f,1.3f,4.4f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.36f,1.26f,4.38f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.36f,1.26f,4.32f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.65f,1.3f,4.3f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.65f,1.2f,4.4f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.36f,1.2f,4.38f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.36f,1.26f,4.38f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.65f,1.3f,4.4f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.65f,1.2f,4.4f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.36f,1.2f,4.32f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.36f,1.26f,4.32f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.65f,1.3f,4.4f);//esquina superior izquierda
        
        //ducha8
        glTexCoord2d(0,0);glVertex3f (18.65f,1.2f,5.4f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.65f,1.3f,5.4f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.65f,1.3f,5.3f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.65f,1.2f,5.3f);//esquina superior izquierda
       
        glTexCoord2d(0,0);glVertex3f (18.36f,1.2f,5.38f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.36f,1.26f,5.38f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.36f,1.26f,5.32f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.36f,1.2f,5.32f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.65f,1.2f,5.4f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.36f,1.2f,5.38f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.36f,1.2f,5.32f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.65f,1.2f,5.3f);//esquina superior izquierda
     
        glTexCoord2d(0,0);glVertex3f (18.65f,1.3f,5.4f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.36f,1.26f,5.38f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.36f,1.26f,5.32f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.65f,1.3f,5.3f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.65f,1.2f,5.4f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.36f,1.2f,5.38f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.36f,1.26f,5.38f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.65f,1.3f,5.4f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.65f,1.2f,5.4f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.36f,1.2f,5.32f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.36f,1.26f,5.32f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.65f,1.3f,5.4f);//esquina superior izquierda
        
        
        glEnd();
    }
}
