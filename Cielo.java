
package vmorales;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glTexCoord2d;
import static org.lwjgl.opengl.GL11.glVertex3f;


public class Cielo {
    
    public void drawCielo (){
        
         glBegin (GL_QUADS);
         
        glTexCoord2d(0,0);glVertex3f (-50.5f,-0.5f,-50f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (50.5f,-0.5f,-50f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (50.5f,70.5f,-50f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-50.5f,70.5f,-50f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-50.5f,-0.5f,50f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (50.5f,-0.5f,50f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (50.5f,70.5f,50f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-50.5f,70.5f,50f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-50.5f,-0.5f,-50f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-50.5f,-0.5f,50f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-50.5f,70.5f,50f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-50.5f,70.5f,-50f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (50.5f,-0.5f,-50f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (50.5f,-0.5f,50f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (50.5f,70.5f,50f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (50.5f,70.5f,-50f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-50.5f,70.5f,-50f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-50.5f,70.5f,50f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (50.5f,70.5f,50f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (50.5f,70.5f,-50f);//esquina superior izquierda
        
        glEnd();
    }
    
    public void drawTierra (){
        
        glBegin (GL_QUADS);
        
        glTexCoord2d(0,0);glVertex3f (-60.5f,-0.58f,-60f);//esquina inferior izquierda
        glTexCoord2d(0,50);glVertex3f (-60.5f,-0.58f,60f);//esquina inferior derecha
        glTexCoord2d(50,50);glVertex3f (60.5f,-0.58f,60f);//esquina superior derecha
        glTexCoord2d(50,0);glVertex3f (60.5f,-0.58f,-60f);//esquina superior izquierda
        
        glEnd();
    }
}
