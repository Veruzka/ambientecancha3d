/*
 *  https://www.youtube.com/watch?v=hrByOfHTilI
 */
package vmorales;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.util.glu.GLU.*;


public class Camara {

    private float x,y,z,rx,ry,rz; //variables para moverse en el espacio 3D (r es rotacion)
    private float fov, aspect, near, far;
    
    public Camara (float fov, float aspect, float near, float far){
        
////inicializar posicion de la camara
//        x=3; y=-0.7f; z=-8.5f;
//        rx=0;ry=90;rz=0;

        //Posicion general
            x=23; y=-14f; z=-8.5f;
            rx=30;ry=90;rz=0;
   
//          //Posicion Rehabilitacion
//         x=-6; y=-0.7f; z=-12.5f;
//         rx=0;ry=90;rz=0;
//          //Posicion Sobre Cancha
//            x=7; y=-14f; z=-8.5f;
//            rx=90;ry=90;rz=0;

           //Posicion baño
//            x=-16; y=-0.7f; z=-7f;
//            rx=0;ry=90;rz=0;

            //Posicion baño
//            x=-5; y=-0.7f; z=-5f;
//            rx=0;ry=90;rz=0;
            

        this.fov=fov; //ángulo de dirección de la cámara
        this.aspect=aspect;
        this.near=near;
        this.far=far;
        
        initProjection();
    }
    
    //método para preparar las matrices de proyección y modelado de objetos 
    private void initProjection (){
        glMatrixMode (GL_PROJECTION);
        glLoadIdentity (); //para resetear matriz de proyección
        //activar perspectiva para crear la cámara
        gluPerspective(fov, aspect, near, far);//valores que vamos a ingresar para crear cámara
        glMatrixMode (GL_MODELVIEW);//crea matriz de modelado
        glLoadIdentity ();        
    }

    //método para movernos en el espacio 3d
    public void useView (){
        //llamar matrices de rotacion
        glRotatef(rx,1,0,0);//rotacion en x
        glRotatef(ry,0,1,0);//rotacion en y
        glRotatef(rz,0,0,1);//rotacion en z
        //traslacion
        glTranslatef (x,y,z);
        
    }

    
    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public float getRx() {
        return rx;
    }

    public void setRx(float rx) {
        this.rx = rx;
    }

    public float getRy() {
        return ry;
    }

    public void setRy(float ry) {
        this.ry = ry;
    }

    public float getRz() {
        return rz;
    }

    public void setRz(float rz) {
        this.rz = rz;
    }
    
    //Métodos adicionales que nos permiten movernos en el espacio
   // amt es parametro para calcular el movimiento
   public void moveX (float amt){
      z += amt*Math.sin (Math.toRadians(ry));
      x += amt*Math.cos (Math.toRadians(ry));
   }
    public void moveY (float amt){
      y +=amt;
   }
     public void moveZ (float amt){
      z += amt*Math.sin (Math.toRadians(ry+90));
      x += amt*Math.cos (Math.toRadians(ry+90));
   }
      public void rotateX (float amt){
      rx +=amt;
   }
       public void rotateY (float amt){
       ry +=amt;
   }
        public void rotateZ (float amt){
       rz +=amt;
   }
        
        public void vistaGeneral (){
        //Posicion General
            x=23; y=-14f; z=-8.5f;
            rx=30;ry=90;rz=0;
        }
        
        public void vistaJugador (){
        //Posicion Jugador
            x=18; y=-0.7f; z=-8.5f;
            rx=0;ry=90;rz=0;
            
        }
        public void vistaObjeto (){
        //Posicion Jugador
            x=7f; y=-0.1f; z=-14f;
            rx=0;ry=90;rz=0;
            
        }
        public void vistaPrincipal (){
        //inicializar posicion de la camara
            x=3; y=-0.7f; z=-8.5f;
            rx=0;ry=90;rz=0;
        }
        
        public void vistaCancha(){
        //Posicion Sobre Cancha
            x=7; y=-14f; z=-8.5f;
            rx=90;ry=90;rz=0;
        }
        

}
