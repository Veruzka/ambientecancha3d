
package vmorales;

import static org.lwjgl.opengl.GL11.GL_POLYGON;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glTexCoord2d;
import static org.lwjgl.opengl.GL11.glVertex2f;
import static org.lwjgl.opengl.GL11.glVertex3f;


public class Rehabilitacion {
    
    public static float Pi=3.1416f; 
    public void drawCaminadoras (float size){
        
        glBegin (GL_QUADS);  
        
        //CAMINADORA 1
        
        glTexCoord2d(0,0);glVertex3f (0.5f,-0.4f,15.5f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.5f,-0.4f,16.5f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.2f,-0.3f,16.5f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.2f,-0.3f,15.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.5f,-0.5f,16.5f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.5f,-0.4f,16.5f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.2f,-0.3f,16.5f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.2f,-0.5f,16.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.5f,-0.5f,15.5f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.5f,-0.4f,15.5f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.2f,-0.3f,15.5f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.2f,-0.5f,15.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.5f,-0.5f,15.5f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.5f,-0.5f,16.5f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.5f,-0.4f,16.5f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.5f,-0.4f,15.5f);//esquina superior izquierda
            
        glTexCoord2d(0,0);glVertex3f (0.5f,0.45f,15.7f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.5f,0.45f,16.3f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.5f,0.4f,16.3f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.5f,0.4f,15.7f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.25f,0.5f,15.7f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.25f,0.5f,16.3f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.25f,0.45f,16.3f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.25f,0.45f,15.7f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.25f,0.5f,15.7f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.5f,0.45f,15.7f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.5f,0.40f,15.7f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.25f,0.45f,15.7f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.25f,0.5f,16.3f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.5f,0.45f,16.3f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.5f,0.40f,16.3f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.25f,0.45f,16.3f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.25f,0.5f,16.3f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.5f,0.4f,16.3f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.5f,0.4f,15.7f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.25f,0.45f,15.7f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.2f,-0.5f,15.8f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.2f,-0.5f,16.2f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.2f,0.5f,16.2f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.2f,0.5f,15.8f);//esquina superior izquierda

        //CAMINADORA 2
        
        glTexCoord2d(0,0);glVertex3f (0.5f,-0.4f,13.5f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.5f,-0.4f,14.5f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.2f,-0.3f,14.5f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.2f,-0.3f,13.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.5f,-0.5f,14.5f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.5f,-0.4f,14.5f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.2f,-0.3f,14.5f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.2f,-0.5f,14.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.5f,-0.5f,13.5f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.5f,-0.4f,13.5f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.2f,-0.3f,13.5f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.2f,-0.5f,13.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.5f,-0.5f,13.5f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.5f,-0.5f,14.5f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.5f,-0.4f,14.5f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.5f,-0.4f,13.5f);//esquina superior izquierda
            
        glTexCoord2d(0,0);glVertex3f (0.5f,0.45f,13.7f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.5f,0.45f,14.3f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.5f,0.4f,14.3f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.5f,0.4f,13.7f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.25f,0.5f,13.7f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.25f,0.5f,14.3f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.25f,0.45f,14.3f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.25f,0.45f,13.7f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.25f,0.5f,13.7f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.5f,0.45f,13.7f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.5f,0.40f,13.7f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.25f,0.45f,13.7f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.25f,0.5f,14.3f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.5f,0.45f,14.3f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.5f,0.40f,14.3f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.25f,0.45f,14.3f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.25f,0.5f,14.3f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.5f,0.4f,14.3f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.5f,0.4f,13.7f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.25f,0.45f,13.7f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.2f,-0.5f,13.8f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.2f,-0.5f,14.2f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.2f,0.5f,14.2f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.2f,0.5f,13.8f);//esquina superior izquierda

        //CAMINADORA 3
        
        glTexCoord2d(0,0);glVertex3f (0.5f,-0.4f,11.5f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.5f,-0.4f,12.5f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.2f,-0.3f,12.5f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.2f,-0.3f,11.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.5f,-0.5f,12.5f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.5f,-0.4f,12.5f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.2f,-0.3f,12.5f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.2f,-0.5f,12.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.5f,-0.5f,11.5f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.5f,-0.4f,11.5f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.2f,-0.3f,11.5f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.2f,-0.5f,11.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.5f,-0.5f,11.5f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.5f,-0.5f,12.5f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.5f,-0.4f,12.5f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.5f,-0.4f,11.5f);//esquina superior izquierda
            
        glTexCoord2d(0,0);glVertex3f (0.5f,0.45f,11.7f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.5f,0.45f,12.3f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.5f,0.4f,12.3f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.5f,0.4f,11.7f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.25f,0.5f,11.7f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.25f,0.5f,12.3f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.25f,0.45f,12.3f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.25f,0.45f,11.7f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.25f,0.5f,11.7f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.5f,0.45f,11.7f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.5f,0.40f,11.7f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.25f,0.45f,11.7f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.25f,0.5f,12.3f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.5f,0.45f,12.3f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.5f,0.40f,12.3f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.25f,0.45f,12.3f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.25f,0.5f,12.3f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.5f,0.4f,12.3f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.5f,0.4f,11.7f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.25f,0.45f,11.7f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.2f,-0.5f,11.8f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.2f,-0.5f,12.2f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.2f,0.5f,12.2f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.2f,0.5f,11.8f);//esquina superior izquierda
        
        glEnd();
        
         
        
    }
    
    public void drawCir(){
         glBegin(GL_POLYGON);
         
            //caminadora1
            for(int i=0; i<51; i++){
               float z = (float) Math.cos(i*2*Pi/100);
               float y = (float) Math.sin(i*2*Pi/100);
               float x = 1;
               glVertex3f(0.25f*x,0.25f*y+0.5f,0.25f*z+16); 
            }
  
          
             glEnd();
    }
      public void drawCir1(){
          
         glBegin(GL_POLYGON);
         
            //caminadora1
            for(int i=0; i<51; i++){
               float z = (float) Math.cos(i*2*Pi/100);
               float y = (float) Math.sin(i*2*Pi/100);
               float x = 1;
               glVertex3f(0.25f*x,0.25f*y+0.5f,0.25f*z+14); 
            }
  
          
             glEnd();
    }
      
      public void drawCir2(){
          
         glBegin(GL_POLYGON);
         
            //caminadora1
            for(int i=0; i<51; i++){
               float z = (float) Math.cos(i*2*Pi/100);
               float y = (float) Math.sin(i*2*Pi/100);
               float x = 1;
               glVertex3f(0.25f*x,0.25f*y+0.5f,0.25f*z+12); 
            }
  
          
             glEnd();
    }
      
      public void drawPelota(float c,float m){
          
         glBegin(GL_POLYGON);
         
            //caminadora1
            for(int i=0; i<100; i++){
               float z = (float) Math.cos(i*2*Pi/100);
               float y = (float) Math.sin(i*2*Pi/100);
               float x = 1;
               
               glVertex3f((0.1f+m)*x-6,(0.1f+m)*y-0.25f,(0.1f+m)*z+14-c); 
            }
  
          
             glEnd();
    }

      
    public void drawCinta (){
        
        glBegin (GL_QUADS);  
        
        //Cinta 1
        glTexCoord2d(0,0);glVertex3f (2.5f,-0.47f,15.6f);//esquina inferior izquierda
        glTexCoord2d(0,15);glVertex3f (2.5f,-0.47f,16.4f);//esquina inferior derecha
        glTexCoord2d(15,15);glVertex3f (0.3f,-0.42f,16.4f);//esquina superior derecha
        glTexCoord2d(15,0);glVertex3f (0.3f,-0.42f,15.6f);//esquina superior izquierda
        
        //Cinta 2
        glTexCoord2d(0,0);glVertex3f (2.5f,-0.47f,13.6f);//esquina inferior izquierda
        glTexCoord2d(0,15);glVertex3f (2.5f,-0.47f,14.4f);//esquina inferior derecha
        glTexCoord2d(15,15);glVertex3f (0.3f,-0.42f,14.4f);//esquina superior derecha
        glTexCoord2d(15,0);glVertex3f (0.3f,-0.42f,13.6f);//esquina superior izquierda
        
        //Cinta 3
        glTexCoord2d(0,0);glVertex3f (2.5f,-0.47f,11.6f);//esquina inferior izquierda
        glTexCoord2d(0,15);glVertex3f (2.5f,-0.47f,12.4f);//esquina inferior derecha
        glTexCoord2d(15,15);glVertex3f (0.3f,-0.42f,12.4f);//esquina superior derecha
        glTexCoord2d(15,0);glVertex3f (0.3f,-0.42f,11.6f);//esquina superior izquierda
        
        glEnd();
        
    }
    
    public void drawColchonetas (){
        
        glBegin (GL_QUADS); 
        
        glTexCoord2d(0,0);glVertex3f (9f,-0.45f,15.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (9f,-0.45f,16.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (11f,-0.45f,16.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (11f,-0.45f,15.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (9f,-0.35f,15.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (9f,-0.35f,16.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (11f,-0.35f,16.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (11f,-0.35f,15.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (9f,-0.45f,15.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (9f,-0.35f,15.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (11f,-0.35f,15.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (11f,-0.45f,15.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (9f,-0.25f,15.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (9f,-0.25f,16.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (11f,-0.25f,16.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (11f,-0.25f,15.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (9f,-0.35f,15.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (9f,-0.25f,15.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (11f,-0.25f,15.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (11f,-0.35f,15.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (9f,-0.15f,15.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (9f,-0.15f,16.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (11f,-0.15f,16.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (11f,-0.15f,15.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (9f,-0.25f,15.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (9f,-0.15f,15.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (11f,-0.15f,15.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (11f,-0.25f,15.5f);//esquina superior izquierda

        glTexCoord2d(0,0);glVertex3f (9f,-0.05f,15.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (9f,-0.05f,16.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (11f,-0.05f,16.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (11f,-0.05f,15.5f);//esquina superior izquierda

        glTexCoord2d(0,0);glVertex3f (9f,-0.15f,15.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (9f,-0.05f,15.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (11f,-0.05f,15.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (11f,-0.15f,15.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (9f,0.05f,15.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (9f,0.05f,16.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (11f,0.05f,16.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (11f,0.05f,15.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (9f,-0.05f,15.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (9f,0.05f,15.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (11f,0.05f,15.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (11f,-0.05f,15.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (9f,-0.45f,15.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (9f,-0.45f,16.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (9f,-0.35f,16.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (9f,-0.35f,15.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (9f,-0.35f,15.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (9f,-0.35f,16.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (9f,-0.25f,16.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (9f,-0.25f,15.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (9f,-0.25f,15.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (9f,-0.25f,16.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (9f,-0.15f,16.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (9f,-0.15f,15.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (9f,-0.15f,15.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (9f,-0.15f,16.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (9f,-0.05f,16.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (9f,-0.05f,15.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (9f,-0.05f,15.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (9f,-0.05f,16.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (9f,0.05f,16.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (9f,0.05f,15.5f);//esquina superior izquierda
        
         glEnd();
    }
    
    public void drawConos(){
        
        glBegin (GL_QUADS);
        
        //cono 1
        
        glTexCoord2d(0,0);glVertex3f (12f,-0.45f,12.15f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (12f,-0.45f,12f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (12.075f,-0.25f,12.06f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (12.075f,-0.25f,12.09f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (12.15f,-0.45f,12.15f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (12.15f,-0.45f,12f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (12.075f,-0.25f,12.06f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (12.075f,-0.25f,12.09f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (12f,-0.45f,12.15f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (12.06f,-0.25f,12.075f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (12.09f,-0.25f,12.075f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (12.15f,-0.45f,12.15f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (12f,-0.45f,12f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (12.06f,-0.25f,12.075f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (12.09f,-0.25f,12.075f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (12.15f,-0.45f,12f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (11.95f,-0.45f,12.2f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (11.95f,-0.45f,11.95f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (12.2f,-0.45f,11.95f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (12.2f,-0.45f,12.2f);//esquina superior izquierda
        
        //cono 2
        
        glTexCoord2d(0,0);glVertex3f (12.5f,-0.45f,12.65f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (12.5f,-0.45f,12.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (12.575f,-0.25f,12.56f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (12.575f,-0.25f,12.59f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (12.65f,-0.45f,12.65f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (12.65f,-0.45f,12.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (12.575f,-0.25f,12.56f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (12.575f,-0.25f,12.59f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (12.5f,-0.45f,12.65f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (12.56f,-0.25f,12.575f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (12.59f,-0.25f,12.575f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (12.65f,-0.45f,12.65f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (12.5f,-0.45f,12.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (12.56f,-0.25f,12.575f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (12.59f,-0.25f,12.575f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (12.65f,-0.45f,12.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (12.45f,-0.45f,12.7f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (12.45f,-0.45f,12.45f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (12.7f,-0.45f,12.45f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (12.7f,-0.45f,12.7f);//esquina superior izquierda
        
        //cono3
        
        glTexCoord2d(0,0);glVertex3f (12f,-0.45f,12.65f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (12f,-0.45f,12.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (12.075f,-0.25f,12.56f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (12.075f,-0.25f,12.59f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (12.15f,-0.45f,12.65f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (12.15f,-0.45f,12.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (12.075f,-0.25f,12.56f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (12.075f,-0.25f,12.59f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (12f,-0.45f,12.65f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (12.06f,-0.25f,12.575f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (12.09f,-0.25f,12.575f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (12.15f,-0.45f,12.65f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (12f,-0.45f,12.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (12.06f,-0.25f,12.575f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (12.09f,-0.25f,12.575f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (12.15f,-0.45f,12.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (11.95f,-0.45f,12.7f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (11.95f,-0.45f,12.45f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (12.2f,-0.45f,12.45f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (12.2f,-0.45f,12.7f);//esquina superior izquierda
        
        //cono4
        
        glTexCoord2d(0,0);glVertex3f (12.5f,-0.45f,11.85f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (12.5f,-0.45f,11.7f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (12.575f,-0.25f,11.76f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (12.575f,-0.25f,11.79f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (12.65f,-0.45f,11.85f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (12.65f,-0.45f,11.7f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (12.575f,-0.25f,11.76f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (12.575f,-0.25f,11.79f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (12.5f,-0.45f,11.85f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (12.56f,-0.25f,11.775f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (12.59f,-0.25f,11.775f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (12.65f,-0.45f,11.85f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (12.5f,-0.45f,11.7f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (12.56f,-0.25f,11.775f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (12.59f,-0.25f,11.775f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (12.65f,-0.45f,11.7f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (12.45f,-0.45f,11.9f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (12.45f,-0.45f,11.65f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (12.7f,-0.45f,11.65f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (12.7f,-0.45f,11.9f);//esquina superior izquierda
        
        
        //cono C1
        
        glTexCoord2d(0,0);glVertex3f (-5f,-0.45f,12.15f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-5f,-0.45f,12f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-5.075f,-0.25f,12.06f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-5.075f,-0.25f,12.09f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-5.15f,-0.45f,12.15f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-5.15f,-0.45f,12f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-5.075f,-0.25f,12.06f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-5.075f,-0.25f,12.09f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-5f,-0.45f,12.15f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-5.06f,-0.25f,12.075f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-5.09f,-0.25f,12.075f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-5.15f,-0.45f,12.15f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-5f,-0.45f,12f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-5.06f,-0.25f,12.075f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-5.09f,-0.25f,12.075f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-5.15f,-0.45f,12f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-4.95f,-0.45f,12.2f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-4.95f,-0.45f,11.95f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-5.2f,-0.45f,11.95f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-5.2f,-0.45f,12.2f);//esquina superior izquierda
        
        //cono C2
        
        glTexCoord2d(0,0);glVertex3f (-5f,-0.45f,11.15f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-5f,-0.45f,11f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-5.075f,-0.25f,11.06f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-5.075f,-0.25f,11.09f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-5.15f,-0.45f,11.15f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-5.15f,-0.45f,11f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-5.075f,-0.25f,11.06f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-5.075f,-0.25f,11.09f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-5f,-0.45f,11.15f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-5.06f,-0.25f,11.075f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-5.09f,-0.25f,11.075f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-5.15f,-0.45f,11.15f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-5f,-0.45f,11f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-5.06f,-0.25f,11.075f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-5.09f,-0.25f,11.075f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-5.15f,-0.45f,11f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-4.95f,-0.45f,11.2f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-4.95f,-0.45f,10.95f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-5.2f,-0.45f,10.95f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-5.2f,-0.45f,11.2f);//esquina superior izquierda
        
        //cono C3
        
       glTexCoord2d(0,0);glVertex3f (-5f,-0.45f,10.15f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-5f,-0.45f,10f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-5.075f,-0.25f,10.06f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-5.075f,-0.25f,10.09f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-5.15f,-0.45f,10.15f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-5.15f,-0.45f,10f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-5.075f,-0.25f,10.06f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-5.075f,-0.25f,10.09f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-5f,-0.45f,10.15f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-5.06f,-0.25f,10.075f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-5.09f,-0.25f,10.075f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-5.15f,-0.45f,10.15f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-5f,-0.45f,10f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-5.06f,-0.25f,10.075f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-5.09f,-0.25f,10.075f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-5.15f,-0.45f,10f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-4.95f,-0.45f,10.2f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-4.95f,-0.45f,9.95f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-5.2f,-0.45f,9.95f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-5.2f,-0.45f,10.2f);//esquina superior izquierda
        
        //cono C4
        
        glTexCoord2d(0,0);glVertex3f (-5f,-0.45f,13.15f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-5f,-0.45f,13f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-5.075f,-0.25f,13.06f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-5.075f,-0.25f,13.09f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-5.15f,-0.45f,13.15f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-5.15f,-0.45f,13f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-5.075f,-0.25f,13.06f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-5.075f,-0.25f,13.09f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-5f,-0.45f,13.15f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-5.06f,-0.25f,13.075f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-5.09f,-0.25f,13.075f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-5.15f,-0.45f,13.15f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-5f,-0.45f,13f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-5.06f,-0.25f,13.075f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-5.09f,-0.25f,13.075f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-5.15f,-0.45f,13f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-4.95f,-0.45f,13.2f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-4.95f,-0.45f,12.95f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-5.2f,-0.45f,12.95f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-5.2f,-0.45f,13.2f);//esquina superior izquierda
        
        //cono C5
        
        glTexCoord2d(0,0);glVertex3f (-5f,-0.45f,14.15f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-5f,-0.45f,14f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-5.075f,-0.25f,14.06f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-5.075f,-0.25f,14.09f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-5.15f,-0.45f,14.15f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-5.15f,-0.45f,14f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-5.075f,-0.25f,14.06f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-5.075f,-0.25f,14.09f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-5f,-0.45f,14.15f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-5.06f,-0.25f,14.075f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-5.09f,-0.25f,14.075f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-5.15f,-0.45f,14.15f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-5f,-0.45f,14f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-5.06f,-0.25f,14.075f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-5.09f,-0.25f,14.075f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-5.15f,-0.45f,14f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-4.95f,-0.45f,14.2f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-4.95f,-0.45f,13.95f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-5.2f,-0.45f,13.95f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-5.2f,-0.45f,14.2f);//esquina superior izquierda
        glEnd();
    }
    
    public void drawBancas(){
        glBegin (GL_QUADS);
        
        glTexCoord2d(0,0);glVertex3f (9f,-0.2f,11.3f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (9f,-0.2f,11.7f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (11f,-0.2f,11.7f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (11f,-0.2f,11.3f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (8f,-0.2f,10.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (8f,-0.2f,11f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (11.3f,-0.2f,11f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (11.3f,-0.2f,10.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (8f,-0.2f,6.3f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (8f,-0.2f,6.7f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (11.3f,-0.2f,6.7f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (11.3f,-0.2f,6.3f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (9f,-0.5f,11.3f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (9f,-0.5f,11.7f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (9f,-0.2f,11.7f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (9f,-0.2f,11.3f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (11f,-0.5f,11.3f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (11f,-0.5f,11.7f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (11f,-0.2f,11.7f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (11f,-0.2f,11.3f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (11.3f,-0.5f,10.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (11.3f,-0.5f,11f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (11.3f,-0.2f,11f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (11.3f,-0.2f,10.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (8f,-0.5f,10.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (8f,-0.5f,11f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (8f,-0.2f,11f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (8f,-0.2f,10.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (11.3f,-0.5f,6.3f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (11.3f,-0.5f,6.7f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (11.3f,-0.2f,6.7f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (11.3f,-0.2f,6.3f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (8f,-0.5f,6.3f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (8f,-0.5f,6.7f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (8f,-0.2f,6.7f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (8f,-0.2f,6.3f);//esquina superior izquierda
        glEnd();
    }
    
}
