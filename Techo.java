
package vmorales;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glNormal3f;
import static org.lwjgl.opengl.GL11.glTexCoord2d;
import static org.lwjgl.opengl.GL11.glVertex3f;


public class Techo {
    
    //techo interno
    public void drawTecho (float size){
        
        glBegin (GL_QUADS);
        glNormal3f(0,1,0);//activacion de normales para luces
        glTexCoord2d(0,0);glVertex3f (-0.3f,1.5f,0);//esquina frontal izquierda
        glTexCoord2d(0,25);glVertex3f (-0.3f,1.5f,17);//esquina frontal derecha
        glTexCoord2d(25,25);glVertex3f (18.7f,1.5f,17);//esquina posterior derecha
        glTexCoord2d(25,0);glVertex3f (18.7f,1.5f,0);//esquina posterior izquierda 
        glEnd();

    }
    
    //techo externo
    public void drawTechoext (){
        
         glBegin (GL_QUADS);
         
         //parte superior
        glTexCoord2d(0,0);glVertex3f (7.5f,4,6.5f);//esquina frontal izquierda
        glTexCoord2d(0,7);glVertex3f (7.5f,4,10.5f);//esquina frontal derecha
        glTexCoord2d(7,7);glVertex3f (11.5f,4,10.5f);//esquina posterior derecha
        glTexCoord2d(7,0);glVertex3f (11.5f,4,6.5f);//esquina posterior izquierda 
        //parte posterior
        glTexCoord2d(0,0);glVertex3f (11.5f,4,6.5f);//esquina frontal izquierda
        glTexCoord2d(0,7);glVertex3f (11.5f,4,10.5f);//esquina frontal derecha
        glTexCoord2d(7,7);glVertex3f (19,1.5f,17.3f);//esquina posterior derecha
        glTexCoord2d(7,0);glVertex3f (19,1.5f,0);//esquina posterior izquierda 
        //parte frontal
        glTexCoord2d(0,0);glVertex3f (-0.3f,1.5f,0);//esquina frontal izquierda
        glTexCoord2d(0,7);glVertex3f (-0.3f,1.5f,17.3f);//esquina frontal derecha
        glTexCoord2d(7,7);glVertex3f (7.5f,4,10.5f);//esquina posterior derecha
        glTexCoord2d(7,0);glVertex3f (7.5f,4,6.5f);//esquina posterior izquierda 
        //parte lateral derecha
        glTexCoord2d(0,0);glVertex3f (7.5f,4,10.5f);//esquina frontal izquierda
        glTexCoord2d(0,7);glVertex3f (-0.3f,1.5f,17.3f);//esquina frontal derecha
        glTexCoord2d(7,7);glVertex3f (19,1.5f,17.3f);//esquina posterior derecha
        glTexCoord2d(7,0);glVertex3f (11.5f,4,10.5f);//esquina posterior izquierda 
        //parte lateral izquierda
        glTexCoord2d(0,0);glVertex3f (-0.3f,1.5f,0);//esquina frontal izquierda
        glTexCoord2d(0,7);glVertex3f (7.5f,4,6.5f);//esquina frontal derecha
        glTexCoord2d(7,7);glVertex3f (11.5f,4,6.5f);//esquina posterior derecha
        glTexCoord2d(7,0);glVertex3f (19,1.5f,0);//esquina posterior izquierda 
        
        glEnd();
        
    }
    
    public void drawTechoTribuna () {
        
        glBegin (GL_QUADS);
        
        glTexCoord2d(0,0);glVertex3f (-17.6f,1.8f,-3.2f);//esquina inferior izquierda
        glTexCoord2d(0,30);glVertex3f (-17.6f,1.8f,21.2f);//esquina inferior derecha
        glTexCoord2d(30,30);glVertex3f (-19.7f,2.3f,21.2f);//esquina superior derecha
        glTexCoord2d(30,0);glVertex3f (-19.7f,2.3f,-3.2f);//esquina superior izquierda
        
        glEnd();
        
    }
}
