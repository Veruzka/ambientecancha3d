
package vmorales;

import static org.lwjgl.opengl.GL11.*;


public class Puertas {
    //puerta principal
    public void drawPuertaPrincipal(float size,float TrasladaZ){
        

        glBegin (GL_QUADS);        
        //Cara frontal izquierda
        glTexCoord2d(0,0);glVertex3f (0.02f,-0.5f,7.4f-TrasladaZ);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (0.02f,-0.5f,8.5f-TrasladaZ);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (0.02f,1.5f,8.5f-TrasladaZ);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (0.02f,1.5f,7.4f-TrasladaZ);//esquina superior izquierda 
        glEnd();
        
        glBegin (GL_QUADS);        
        //Cara frontal derecha
        glTexCoord2d(0,0);glVertex3f (0.02f,-0.5f,8.5f+TrasladaZ);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (0.02f,-0.5f,9.5f+TrasladaZ);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (0.02f,1.5f,9.5f+TrasladaZ);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (0.02f,1.5f,8.5f+TrasladaZ);//esquina superior izquierda 
        glEnd();

    }
}
