/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vmorales;

import java.util.Scanner;

/**
 *
 * @author redes1
 */
public class Escenarios {

    Scanner lector = new Scanner(System.in);

    public String[][] escenarioUno() {
        String[][] matriz = {
            {"lbf","fbi","f","f","f","f","f","f","p1","","f","f","f","f","f","f","fbd","lbf"},
            { "l", " "," "," "," "," ","lb1", " "," ", " "," ","lb1", " ", " ", " "," "," ","l"},
            { "l", " "," "," "," "," ","l", " "," ", " "," ","l", " ", " ", " "," "," ","l"},
            { "l", " "," "," "," "," ","l", " "," ", " "," ","l", " ", " ", " "," "," ","l"},
            { "l", " "," "," "," "," ","l", " "," ", " "," ","l", " ", " ", " "," "," ","l"},
            { "l", " "," "," "," "," ","l", " "," ", " "," ","l", " ", " ", " "," "," ","l"},
            { "l", " "," "," "," "," ","", " "," ", " "," ","", " ", " ", " "," "," ","l"},
            { "l", " "," "," "," "," "," ", " "," ", " "," "," ", " ", " ", " "," "," ","l"},
            { "l", " "," "," "," "," ","l", " "," ", " "," ","l", " ", " ", " "," "," ","l"},
            { "l", " "," "," "," "," ","l", " "," ", " "," ","l", " ", " ", " "," "," ","l"},
            { "l", " "," "," "," "," ","l", " "," ", " "," ","l", " ", " ", " "," "," ","l"},
            { "l", " "," "," "," "," ","l", " "," ", " "," ","l", " ", " ", " "," "," ","l"},
            { "l", " "," "," "," "," ","lb2", " "," ", " "," ","lb2", " ", " ", " "," "," ","l"},
            { "l","fb1","f","f","f",""," ","fb1","f", "f","fbd","l", " ", " ", " "," "," ","l"},
            { "plb", " "," "," "," "," ","lb1b", " "," ", " "," ","l", " ", " ", " "," "," ","l"},
            { "plb", " "," "," "," "," ","plb", " "," ", " "," ","l", " ", " ", " "," "," ","l"},
            { "plb", " "," "," "," "," ","", " "," ", " "," ","l", " ", " ", " "," "," ","l"},
            { "plb", " "," "," "," "," ","plb", " "," ", " "," ","l", " ", " ", " "," "," ","l"},
            { "plb", " "," "," "," "," ","lb2b", " "," ", " "," ","lb2", " ", " ", " "," "," ","l"},
            {"lbpb","fbib","pfb","pfb","pfb","pfb","pfb","pfb","pfb","pfb","pfb","f","f","f","f","f","fbd","lbp"}
        };
        return matriz;
    }

   

    public int filaasterico(String[][] matriz) {
        int fila = 0;
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                if (matriz[i][j].equals("*")) {
                    fila = i;
                }
            }
        }
        return fila;
    }
    public int columasterico(String[][] matriz) {
        int fila = 0;
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                if (matriz[i][j].equals("*")) {
                    fila = j;
                }
            }
        }
        return fila;
    }
     int [] arreglo= new int[2];
     public int [] posX(String [][]matriz){
         for (int i = 0; i < matriz.length; i++) {
             for (int j = 0; j < matriz[0].length; j++) {
                 if (matriz[i][j].equals("x")) {
                     arreglo[0]=i;
                     arreglo[1]=j;
                 }
             }
         }
         return arreglo;
     }
}
