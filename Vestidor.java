
package vmorales;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glNormal3f;
import static org.lwjgl.opengl.GL11.glTexCoord2d;
import static org.lwjgl.opengl.GL11.glVertex3f;


public class Vestidor {
    
    
    public void drawVestidores (float size){
        
        //paredes vestidor
        glBegin (GL_QUADS);  
        
        glTexCoord2d(0,0);glVertex3f (0.05f,-size/2,0);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.05f,-size/2,6);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.05f,1.5f,6);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.05f,1.5f,0);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.05f,-size/2,0.35f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (13,-size/2,0.35f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (13,1.5f,0.35f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.05f,1.5f,0.35f);//esquina superior izquierda
   
  
        glEnd();
            
    }
    
    public void vestseparaciones (){
        glBegin (GL_QUADS); 
        
        //vestidores lado izquierdo
        
        //separacion superior 
        glTexCoord2d(0,0);glVertex3f (0.3f,1.15f,0);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.3f,1.15f,6);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.3f,1.2f,6);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.3f,1.2f,0);//esquina superior izquierda

        glTexCoord2d(0,0);glVertex3f (0.3f,1.15f,0);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.3f,1.15f,6);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.05f,1.15f,6);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.05f,1.15f,0);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.3f,1.2f,0);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.3f,1.2f,6);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.05f,1.2f,6);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.05f,1.2f,0);//esquina superior izquierda
        
        //separacion inferior
        glTexCoord2d(0,0);glVertex3f (0.3f,0.05f,0);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.3f,0.05f,6);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.05f,0.05f,6);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.05f,0.05f,0);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.3f,-0.5f,0);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.3f,-0.5f,6);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.3f,0.05f,6);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.3f,0.05f,0);//esquina superior izquierda
        
       //separacion vertical
        glTexCoord2d(0,0);glVertex3f (0.3f,0.05f,5.15f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.3f,0.05f,5.1f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.3f,1.2f,5.1f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.3f,1.2f,5.15f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.3f,0.05f,5.15f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.05f,0.05f,5.15f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.05f,1.2f,5.15f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.3f,1.2f,5.15f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.3f,0.05f,5.1f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.05f,0.05f,5.1f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.05f,1.2f,5.1f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.3f,1.2f,5.1f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.3f,0.05f,4.3f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.3f,0.05f,4.25f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.3f,1.2f,4.25f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.3f,1.2f,4.3f);//esquina superior izquierda

        glTexCoord2d(0,0);glVertex3f (0.3f,0.05f,4.3f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.05f,0.05f,4.3f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.05f,1.2f,4.3f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.3f,1.2f,4.3f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.3f,0.05f,4.25f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.05f,0.05f,4.25f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.05f,1.2f,4.25f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.3f,1.2f,4.25f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.3f,0.05f,3.4f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.3f,0.05f,3.35f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.3f,1.2f,3.35f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.3f,1.2f,3.4f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.3f,0.05f,3.4f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.05f,0.05f,3.4f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.05f,1.2f,3.4f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.3f,1.2f,3.4f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.3f,0.05f,3.35f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.05f,0.05f,3.35f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.05f,1.2f,3.35f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.3f,1.2f,3.35f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.3f,0.05f,2.45f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.3f,0.05f,2.4f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.3f,1.2f,2.4f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.3f,1.2f,2.45f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.3f,0.05f,2.45f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.05f,0.05f,2.45f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.05f,1.2f,2.45f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.3f,1.2f,2.45f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.3f,0.05f,2.4f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.05f,0.05f,2.4f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.05f,1.2f,2.4f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.3f,1.2f,2.4f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.3f,0.05f,1.5f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.3f,0.05f,1.45f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.3f,1.2f,1.45f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.3f,1.2f,1.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.3f,0.05f,1.5f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.05f,0.05f,1.5f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.05f,1.2f,1.5f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.3f,1.2f,1.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.3f,0.05f,1.45f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.05f,0.05f,1.45f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.05f,1.2f,1.45f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.3f,1.2f,1.45f);//esquina superior izquierda

        glTexCoord2d(0,0);glVertex3f (0.3f,0.05f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.05f,0.05f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.05f,1.2f,0.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.3f,1.2f,0.6f);//esquina superior izquierda

        glTexCoord2d(0,0);glVertex3f (0.3f,0.05f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.3f,0.05f,0.35f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.3f,1.2f,0.35f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.3f,1.2f,0.6f);//esquina superior izquierda
        
        //vestidores frente a la puerta
        
        glTexCoord2d(0,0);glVertex3f (1.4f,0.05f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (1.35f,0.05f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (1.35f,1.2f,0.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (1.4f,1.2f,0.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (1.4f,0.05f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (1.4f,0.05f,0.35f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (1.4f,1.2f,0.35f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (1.4f,1.2f,0.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (2.4f,0.05f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (2.35f,0.05f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (2.35f,1.2f,0.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (2.4f,1.2f,0.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (2.4f,0.05f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (2.4f,0.05f,0.35f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (2.4f,1.2f,0.35f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (2.4f,1.2f,0.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (3.4f,0.05f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (3.35f,0.05f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (3.35f,1.2f,0.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (3.4f,1.2f,0.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (3.4f,0.05f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (3.4f,0.05f,0.35f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (3.4f,1.2f,0.35f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (3.4f,1.2f,0.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (4.4f,0.05f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (4.35f,0.05f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (4.35f,1.2f,0.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (4.4f,1.2f,0.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (4.4f,0.05f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (4.4f,0.05f,0.35f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (4.4f,1.2f,0.35f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (4.4f,1.2f,0.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (5.4f,0.05f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (5.35f,0.05f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (5.35f,1.2f,0.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (5.4f,1.2f,0.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (5.4f,0.05f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (5.4f,0.05f,0.35f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (5.4f,1.2f,0.35f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (5.4f,1.2f,0.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (6.4f,0.05f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (6.35f,0.05f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (6.35f,1.2f,0.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (6.4f,1.2f,0.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (6.4f,0.05f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (6.4f,0.05f,0.35f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (6.4f,1.2f,0.35f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (6.4f,1.2f,0.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (7.4f,0.05f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (7.35f,0.05f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (7.35f,1.2f,0.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (7.4f,1.2f,0.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (7.4f,0.05f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (7.4f,0.05f,0.35f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (7.4f,1.2f,0.35f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (7.4f,1.2f,0.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (8.4f,0.05f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (8.35f,0.05f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (8.35f,1.2f,0.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (8.4f,1.2f,0.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (8.4f,0.05f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (8.4f,0.05f,0.35f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (8.4f,1.2f,0.35f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (8.4f,1.2f,0.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (9.4f,0.05f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (9.35f,0.05f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (9.35f,1.2f,0.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (9.4f,1.2f,0.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (9.4f,0.05f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (9.4f,0.05f,0.35f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (9.4f,1.2f,0.35f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (9.4f,1.2f,0.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (10.4f,0.05f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (10.35f,0.05f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (10.35f,1.2f,0.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (10.4f,1.2f,0.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (10.4f,0.05f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (10.4f,0.05f,0.35f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (10.4f,1.2f,0.35f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (10.4f,1.2f,0.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (11.4f,0.05f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (11.35f,0.05f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (11.35f,1.2f,0.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (11.4f,1.2f,0.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (11.4f,0.05f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (11.4f,0.05f,0.35f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (11.4f,1.2f,0.35f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (11.4f,1.2f,0.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (13f,0.05f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (12.35f,0.05f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (12.35f,1.2f,0.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (13f,1.2f,0.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (12.35f,0.05f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (12.35f,0.05f,0.35f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (12.35f,1.2f,0.35f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (12.35f,1.2f,0.6f);//esquina superior izquierda
        
        //separacion superior
        
        glTexCoord2d(0,0);glVertex3f (0.05f,1.15f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (13,1.15f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (13,1.2f,0.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.05f,1.2f,0.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.05f,1.15f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (13,1.15f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (13,1.15f,0.35f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.05f,1.15f,0.35f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.05f,1.2f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (13,1.2f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (13,1.2f,0.35f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.05f,1.2f,0.35f);//esquina superior izquierda
        
        //separacion inferior
        
        glTexCoord2d(0,0);glVertex3f (0.3f,0.05f,0.35f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.3f,0.05f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (13f,0.05f,0.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (13f,0.05f,0.35f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.3f,-0.5f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (13,-0.5f,0.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (13,0.05f,0.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.3f,0.05f,0.6f);//esquina superior izquierda
        
        glEnd();
    }

    public void drawCamisas(){
        
        glBegin (GL_QUADS); 
        
        //camisa1
       
        glTexCoord2d(0,0);glVertex3f (0.6f,0.3f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (1.05f,0.3f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (1.05f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (0.6f,0.95f,0.38f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (1.15f,0.7f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (1.05f,0.7f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (1.05f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (1.15f,0.85f,0.38f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.45f,0.7f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (0.6f,0.7f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (0.6f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (0.45f,0.85f,0.38f);//esquina superior izquierda
        
        //camisa2
       
        glTexCoord2d(0,0);glVertex3f (1.7f,0.3f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (2.15f,0.3f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (2.15f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (1.7f,0.95f,0.38f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (2.25f,0.7f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (2.15f,0.7f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (2.15f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (2.25f,0.85f,0.38f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (1.55f,0.7f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (1.7f,0.7f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (1.7f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (1.55f,0.85f,0.38f);//esquina superior izquierda
        
        //camisa3
       
        glTexCoord2d(0,0);glVertex3f (2.7f,0.3f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (3.15f,0.3f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (3.15f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (2.7f,0.95f,0.38f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (3.25f,0.7f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (3.15f,0.7f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (3.15f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (3.25f,0.85f,0.38f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (2.55f,0.7f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (2.7f,0.7f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (2.7f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (2.55f,0.85f,0.38f);//esquina superior izquierda
        
        //camisa4
       
        glTexCoord2d(0,0);glVertex3f (3.7f,0.3f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (4.15f,0.3f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (4.15f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (3.7f,0.95f,0.38f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (4.25f,0.7f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (4.15f,0.7f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (4.15f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (4.25f,0.85f,0.38f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (3.55f,0.7f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (3.7f,0.7f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (3.7f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (3.55f,0.85f,0.38f);//esquina superior izquierda
        
        //camisa5
       
        glTexCoord2d(0,0);glVertex3f (4.7f,0.3f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (5.15f,0.3f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (5.15f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (4.7f,0.95f,0.38f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (5.25f,0.7f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (5.15f,0.7f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (5.15f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (5.25f,0.85f,0.38f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (4.55f,0.7f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (4.7f,0.7f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (4.7f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (4.55f,0.85f,0.38f);//esquina superior izquierda
        
        //camisa6
       
        glTexCoord2d(0,0);glVertex3f (5.7f,0.3f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (6.15f,0.3f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (6.15f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (5.7f,0.95f,0.38f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (6.25f,0.7f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (6.15f,0.7f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (6.15f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (6.25f,0.85f,0.38f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (5.55f,0.7f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (5.7f,0.7f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (5.7f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (5.55f,0.85f,0.38f);//esquina superior izquierda
        
        //camisa7
       
        glTexCoord2d(0,0);glVertex3f (6.7f,0.3f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (7.15f,0.3f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (7.15f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (6.7f,0.95f,0.38f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (7.25f,0.7f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (7.15f,0.7f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (7.15f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (7.25f,0.85f,0.38f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (6.55f,0.7f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (6.7f,0.7f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (6.7f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (6.55f,0.85f,0.38f);//esquina superior izquierda
        
        //camisa8
       
        glTexCoord2d(0,0);glVertex3f (7.7f,0.3f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (8.15f,0.3f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (8.15f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (7.7f,0.95f,0.38f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (8.25f,0.7f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (8.15f,0.7f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (8.15f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (8.25f,0.85f,0.38f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (7.55f,0.7f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (7.7f,0.7f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (7.7f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (7.55f,0.85f,0.38f);//esquina superior izquierda
        
        //camisa9
       
        glTexCoord2d(0,0);glVertex3f (8.7f,0.3f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (9.15f,0.3f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (9.15f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (8.7f,0.95f,0.38f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (9.25f,0.7f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (9.15f,0.7f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (9.15f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (9.25f,0.85f,0.38f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (8.55f,0.7f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (8.7f,0.7f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (8.7f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (8.55f,0.85f,0.38f);//esquina superior izquierda
        
        //camisa10
       
        glTexCoord2d(0,0);glVertex3f (9.7f,0.3f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (10.15f,0.3f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (10.15f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (9.7f,0.95f,0.38f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (10.25f,0.7f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (10.15f,0.7f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (10.15f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (10.25f,0.85f,0.38f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (9.55f,0.7f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (9.7f,0.7f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (9.7f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (9.55f,0.85f,0.38f);//esquina superior izquierda
        
        //camisa11
       
        glTexCoord2d(0,0);glVertex3f (10.7f,0.3f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (11.15f,0.3f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (11.15f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (10.7f,0.95f,0.38f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (11.25f,0.7f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (11.15f,0.7f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (11.15f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (11.25f,0.85f,0.38f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (10.55f,0.7f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (10.7f,0.7f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (10.7f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (10.55f,0.85f,0.38f);//esquina superior izquierda
        
        //camisa12
       
        glTexCoord2d(0,0);glVertex3f (11.7f,0.3f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (12.15f,0.3f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (12.15f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (11.7f,0.95f,0.38f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (12.25f,0.7f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (12.15f,0.7f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (12.15f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (12.25f,0.85f,0.38f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (11.55f,0.7f,0.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (11.7f,0.7f,0.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (11.7f,0.95f,0.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (11.55f,0.85f,0.38f);//esquina superior izquierda
        
        //camisa13
       
        glTexCoord2d(0,0);glVertex3f (0.08f,0.3f,0.78f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (0.08f,0.3f,1.23f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (0.08f,0.95f,1.23f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (0.08f,0.95f,0.78f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.08f,0.7f,1.33f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (0.08f,0.7f,1.23f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (0.08f,0.95f,1.23f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (0.08f,0.85f,1.33f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.08f,0.7f,0.68f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (0.08f,0.7f,0.78f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (0.08f,0.95f,0.78f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (0.08f,0.85f,0.68f);//esquina superior izquierda
        
        //camisa14
       
        glTexCoord2d(0,0);glVertex3f (0.08f,0.3f,1.73f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (0.08f,0.3f,2.18f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (0.08f,0.95f,2.18f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (0.08f,0.95f,1.73f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.08f,0.7f,2.28f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (0.08f,0.7f,2.18f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (0.08f,0.95f,2.18f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (0.08f,0.85f,2.28f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.08f,0.7f,1.63f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (0.08f,0.7f,1.73f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (0.08f,0.95f,1.73f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (0.08f,0.85f,1.63f);//esquina superior izquierda
        
        //camisa14
       
        glTexCoord2d(0,0);glVertex3f (0.08f,0.3f,2.68f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (0.08f,0.3f,3.13f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (0.08f,0.95f,3.13f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (0.08f,0.95f,2.68f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.08f,0.7f,3.23f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (0.08f,0.7f,3.13f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (0.08f,0.95f,3.13f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (0.08f,0.85f,3.23f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.08f,0.7f,2.58f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (0.08f,0.7f,2.68f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (0.08f,0.95f,2.68f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (0.08f,0.85f,2.58f);//esquina superior izquierda
        
        //camisa15
       
        glTexCoord2d(0,0);glVertex3f (0.08f,0.3f,3.58f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (0.08f,0.3f,4.03f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (0.08f,0.95f,4.03f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (0.08f,0.95f,3.58f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.08f,0.7f,4.13f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (0.08f,0.7f,4.03f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (0.08f,0.95f,4.03f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (0.08f,0.85f,4.13f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.08f,0.7f,3.48f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (0.08f,0.7f,3.58f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (0.08f,0.95f,3.58f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (0.08f,0.85f,3.48f);//esquina superior izquierda
        
        //camisa16
       
        glTexCoord2d(0,0);glVertex3f (0.08f,0.3f,4.48f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (0.08f,0.3f,4.93f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (0.08f,0.95f,4.93f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (0.08f,0.95f,4.48f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.08f,0.7f,5.03f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (0.08f,0.7f,4.93f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (0.08f,0.95f,4.93f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (0.08f,0.85f,5.03f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.08f,0.7f,4.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (0.08f,0.7f,4.48f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (0.08f,0.95f,4.48f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (0.08f,0.85f,4.38f);//esquina superior izquierda
        
        //camisa1
       
        glTexCoord2d(0,0);glVertex3f (0.08f,0.3f,5.38f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (0.08f,0.3f,5.83f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (0.08f,0.95f,5.83f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (0.08f,0.95f,5.38f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.08f,0.7f,5.93f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (0.08f,0.7f,5.83f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (0.08f,0.95f,5.83f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (0.08f,0.85f,5.93f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.08f,0.7f,5.28f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (0.08f,0.7f,5.38f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (0.08f,0.95f,5.38f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (0.08f,0.85f,5.28f);//esquina superior izquierda
        
        glEnd();
    }
    
   
    
    public void vestAsientos(){
        
        glBegin (GL_QUADS); 
        
        glTexCoord2d(0,0);glVertex3f (0.7f,0,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.7f,0,6);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.7f,0.05f,6);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.7f,0.05f,0.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (0.7f,0.05f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0.7f,0.05f,6);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0.3f,0.05f,6);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (0.3f,0.05f,0.6f);//esquina superior izquierda
        
        
        glTexCoord2d(3,0);glVertex3f (0.3f,0.05f,0.6f);//esquina inferior izquierda
        glTexCoord2d(0,0);glVertex3f (0.3f,0.05f,1);//esquina inferior derecha
        glTexCoord2d(0,3);glVertex3f (13f,0.05f,1);//esquina superior derecha
        glTexCoord2d(3,3);glVertex3f (13f,0.05f,0.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (13,0,1);//esquina inferior izquierda
        glTexCoord2d(0,3);glVertex3f (0.3f,0,1);//esquina inferior derecha
        glTexCoord2d(3,3);glVertex3f (0.3f,0.05f,1);//esquina superior derecha
        glTexCoord2d(3,0);glVertex3f (13,0.05f,1);//esquina superior izquierda
        
        glEnd();
    }
}
