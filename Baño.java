
package vmorales;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glTexCoord2d;
import static org.lwjgl.opengl.GL11.glVertex3f;


public class Baño {
    
    public void drawCubiculos (){
        
        glBegin (GL_QUADS);  
        
        //pared metal baño
        glTexCoord2d(0,0);glVertex3f (13f,1f,9.7f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (15f,1f,9.7f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (15f,-0.3f,9.7f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (13f,-0.3f,9.7f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (13f,1f,8.8f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (15f,1f,8.8f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (15f,-0.3f,8.8f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (13f,-0.3f,8.8f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (13f,1f,7.9f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (15f,1f,7.9f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (15f,-0.3f,7.9f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (13f,-0.3f,7.9f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (14.9f,1f,9.7f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (15f,1f,9.7f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (15f,-0.5f,9.7f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (14.9f,-0.5f,9.7f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (14.9f,1f,8.8f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (15f,1f,8.8f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (15f,-0.5f,8.8f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (14.9f,-0.5f,8.8f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (14.9f,1f,7.9f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (15f,1f,7.9f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (15f,-0.5f,7.9f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (14.9f,-0.5f,7.9f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (15f,1f,9.7f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (15f,1f,9.9f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (15f,-0.3f,9.9f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (15f,-0.3f,9.7f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (15f,1f,8.8f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (15f,1f,9f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (15f,-0.3f,9f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (15f,-0.3f,8.8f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (15f,1f,7.9f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (15f,1f,8.1f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (15f,-0.3f,8.1f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (15f,-0.3f,7.9f);//esquina superior izquierda
        
        glEnd();
        
    }
            
    public void drawPuertasBaño () {
        
        glBegin (GL_QUADS);  
        
        glTexCoord2d(0,0);glVertex3f (15f,1f,9.9f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (15f,1f,11f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (15f,-0.3f,11f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (15f,-0.3f,9.9f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (15f,1f,9f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (15f,1f,9.7f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (15f,-0.3f,9.7f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (15f,-0.3f,9f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (15f,1f,8.8f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (15f,1f,8.1f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (15f,-0.3f,8.1f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (15f,-0.3f,8.8f);//esquina superior izquierda

        glEnd();
        
    }
    
    public void drawMezon () {
        
        glBegin (GL_QUADS);
        
        glTexCoord2d(0,0);glVertex3f (19f,0.2f,10.5f);//esquina inferior izquierda
        glTexCoord2d(0,4);glVertex3f (15.5f,0.2f,10.5f);//esquina inferior derecha
        glTexCoord2d(4,4);glVertex3f (15.5f,0.27f,10.5f);//esquina superior derecha
        glTexCoord2d(4,0);glVertex3f (19f,0.27f,10.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (19f,0.27f,10.5f);//esquina inferior izquierda
        glTexCoord2d(0,4);glVertex3f (15.5f,0.27f,10.5f);//esquina inferior derecha
        glTexCoord2d(4,4);glVertex3f (15.5f,0.27f,11f);//esquina superior derecha
        glTexCoord2d(4,0);glVertex3f (19f,0.27f,11f);//esquina superior izquierda
        
        glEnd();
    }
    
    public void drawBaños () {
        
        glBegin (GL_QUADS);
        
        //Baño1 
        
        glTexCoord2d(0,0);glVertex3f (18.6f,0f,6.8f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.6f,0f,6.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.4f,0.15f,6.5f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.4f,0.15f,6.9f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.4f,0.15f,6.9f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.4f,0.15f,6.5f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.4f,0.2f,6.5f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.4f,0.2f,6.9f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.4f,0.15f,6.5f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.7f,0.15f,6.5f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.7f,0.25f,6.5f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.4f,0.25f,6.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.4f,0.15f,6.9f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.7f,0.15f,6.9f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.7f,0.25f,6.9f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.4f,0.25f,6.9f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.4f,0.15f,6.9f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.7f,0.15f,6.9f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.7f,0f,6.8f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.6f,0f,6.8f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.4f,0.15f,6.5f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.7f,0.15f,6.5f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.7f,0f,6.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.6f,0f,6.6f);//esquina superior izquierda
        
        //Baño2 
        
        glTexCoord2d(0,0);glVertex3f (18.6f,0f,7.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.6f,0f,7.4f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.4f,0.15f,7.3f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.4f,0.15f,7.7f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.4f,0.15f,7.7f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.4f,0.15f,7.3f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.4f,0.2f,7.3f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.4f,0.2f,7.7f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.4f,0.15f,7.3f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.7f,0.15f,7.3f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.7f,0.25f,7.3f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.4f,0.25f,7.3f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.4f,0.15f,7.7f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.7f,0.15f,7.7f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.7f,0.25f,7.7f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.4f,0.25f,7.7f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.4f,0.15f,7.7f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.7f,0.15f,7.7f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.7f,0f,7.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.6f,0f,7.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.4f,0.15f,7.3f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.7f,0.15f,7.3f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.7f,0f,7.4f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.6f,0f,7.4f);//esquina superior izquierda
        
        //Baño3 
        
        glTexCoord2d(0,0);glVertex3f (18.6f,0f,8.4f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.6f,0f,8.2f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.4f,0.15f,8.1f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.4f,0.15f,8.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.4f,0.15f,8.5f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.4f,0.15f,8.1f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.4f,0.2f,8.1f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.4f,0.2f,8.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.4f,0.15f,8.1f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.7f,0.15f,8.1f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.7f,0.25f,8.1f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.4f,0.25f,8.1f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.4f,0.15f,8.5f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.7f,0.15f,8.5f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.7f,0.25f,8.5f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.4f,0.25f,8.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.4f,0.15f,8.5f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.7f,0.15f,8.5f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.7f,0f,8.4f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.6f,0f,8.4f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.4f,0.15f,8.1f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.7f,0.15f,8.1f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.7f,0f,8.2f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.6f,0f,8.2f);//esquina superior izquierda
        
        
        //Baño4 
        
        glTexCoord2d(0,0);glVertex3f (18.6f,0f,9.2f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.6f,0f,9f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.4f,0.15f,8.9f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.4f,0.15f,9.3f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.4f,0.15f,9.3f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.4f,0.15f,8.9f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.4f,0.2f,8.9f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.4f,0.2f,9.3f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.4f,0.15f,8.9f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.7f,0.15f,8.9f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.7f,0.25f,8.9f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.4f,0.25f,8.9f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.4f,0.15f,9.3f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.7f,0.15f,9.3f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.7f,0.25f,9.3f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.4f,0.25f,9.3f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.4f,0.15f,9.3f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.7f,0.15f,9.3f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.7f,0f,9.2f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.6f,0f,9.2f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.4f,0.15f,8.9f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.7f,0.15f,8.9f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.7f,0f,9f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.6f,0f,9f);//esquina superior izquierda
        
        //Baño5
        
        glTexCoord2d(0,0);glVertex3f (18.6f,0f,10f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.6f,0f,9.8f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.4f,0.15f,9.7f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.4f,0.15f,10.1f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.4f,0.15f,10.1f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.4f,0.15f,9.7f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.4f,0.2f,9.7f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.4f,0.2f,10.1f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.4f,0.15f,9.7f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.7f,0.15f,9.7f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.7f,0.25f,9.7f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.4f,0.25f,9.7f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.4f,0.15f,10.1f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.7f,0.15f,10.1f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.7f,0.25f,10.1f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.4f,0.25f,10.1f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.4f,0.15f,10.1f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.7f,0.15f,10.1f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.7f,0f,10f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.6f,0f,10f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.4f,0.15f,9.7f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.7f,0.15f,9.7f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.7f,0f,9.8f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.6f,0f,9.8f);//esquina superior izquierda
        
        glEnd();
        
    }
    
    public void drawFondoBaño (){
        
        glBegin (GL_QUADS);
        
        //baño 1
        
        glTexCoord2d(0,0);glVertex3f (18.68f,0f,6.8f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.68f,0f,6.6f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.68f,0.15f,6.5f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.68f,0.15f,6.9f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.68f,0.15f,6.9f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.68f,0.15f,6.5f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.68f,0.4f,6.5f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.68f,0.4f,6.9f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.68f,0.4f,6.9f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.68f,0.4f,6.5f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.68f,0.5f,6.6f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.68f,0.5f,6.8f);//esquina superior izquierda
        
        //baño 2
        
        glTexCoord2d(0,0);glVertex3f (18.68f,0f,7.6f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.68f,0f,7.4f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.68f,0.15f,7.3f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.68f,0.15f,7.7f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.68f,0.15f,7.7f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.68f,0.15f,7.3f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.68f,0.4f,7.3f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.68f,0.4f,7.7f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.68f,0.4f,7.7f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.68f,0.4f,7.3f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.68f,0.5f,7.4f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.68f,0.5f,7.6f);//esquina superior izquierda
        
        //baño 3
        
        glTexCoord2d(0,0);glVertex3f (18.68f,0f,8.4f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.68f,0f,8.2f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.68f,0.15f,8.1f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.68f,0.15f,8.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.68f,0.15f,8.5f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.68f,0.15f,8.1f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.68f,0.4f,8.1f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.68f,0.4f,8.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.68f,0.4f,8.5f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.68f,0.4f,8.1f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.68f,0.5f,8.2f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.68f,0.5f,8.4f);//esquina superior izquierda
        
        //baño 4
        
        glTexCoord2d(0,0);glVertex3f (18.68f,0f,9.2f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.68f,0f,9f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.68f,0.15f,8.9f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.68f,0.15f,9.3f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.68f,0.15f,9.3f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.68f,0.15f,8.9f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.68f,0.4f,8.9f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.68f,0.4f,9.3f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.68f,0.4f,9.3f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.68f,0.4f,8.9f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.68f,0.5f,9f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.68f,0.5f,9.2f);//esquina superior izquierda
        
        //baño 5
        
        glTexCoord2d(0,0);glVertex3f (18.68f,0f,10f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.68f,0f,9.8f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.68f,0.15f,9.7f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.68f,0.15f,10.1f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.68f,0.15f,10.1f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.68f,0.15f,9.7f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.68f,0.4f,9.7f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.68f,0.4f,10.1f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (18.68f,0.4f,10.1f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (18.68f,0.4f,9.7f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (18.68f,0.5f,9.8f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (18.68f,0.5f,10f);//esquina superior izquierda
     
        
        glEnd();
    }
    
    public void drawEspejo (){
        
        glBegin (GL_QUADS);
        
        glTexCoord2d(0,0);glVertex3f (19f,0.25f,10.99f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (15.5f,0.25f,10.99f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (15.5f,1f,10.99f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (19f,1f,10.99f);//esquina superior izquierda
        

        glEnd();
    }
}
