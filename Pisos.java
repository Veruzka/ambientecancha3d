
package vmorales;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glColor3f;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glNormal3f;
import static org.lwjgl.opengl.GL11.glTexCoord2d;
import static org.lwjgl.opengl.GL11.glVertex3f;
import static vmorales.Rehabilitacion.Pi;


public class Pisos {
    
    public void drawPiso(float size){
        glBegin (GL_QUADS);
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,0);//esquina inferior izquierda
        glTexCoord2d(0,25);glVertex3f (-0.3f,-size/2,17);//esquina inferior derecha
        glTexCoord2d(25,25);glVertex3f (12.7f,-size/2,17);//esquina superior derecha
        glTexCoord2d(25,0);glVertex3f (12.7f,-size/2,0);//esquina superior izquierda 
        glEnd();

        
    }
        public void drawPisoTerapia(float size){
        glBegin (GL_QUADS);
        glTexCoord2d(0,0);glVertex3f (12.7f,-size/2,11);//esquina inferior izquierda
        glTexCoord2d(0,50);glVertex3f (12.7f,-size/2,17);//esquina inferior derecha
        glTexCoord2d(50,50);glVertex3f (18.7f,-size/2,17);//esquina superior derecha
        glTexCoord2d(50,0);glVertex3f (18.7f,-size/2,11);//esquina superior izquierda 
        glEnd();
        }
        
        public void drawPisoBaño(float size){
        glBegin (GL_QUADS);
        glTexCoord2d(0,0);glVertex3f (12.7f,-size/2,0);//esquina inferior izquierda
        glTexCoord2d(0,25);glVertex3f (12.7f,-size/2,11);//esquina inferior derecha
        glTexCoord2d(25,25);glVertex3f (18.7f,-size/2,11);//esquina superior derecha
        glTexCoord2d(25,0);glVertex3f (18.7f,-size/2,0);//esquina superior izquierda 
        glEnd();
        
    }
    public void drawPisoExterior(float size){
        //piso lateral derecho
        glBegin (GL_QUADS);
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,17);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-0.3f,-size/2,20);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (20,-size/2,20);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (20,-size/2,17);//esquina superior izquierda 
        //piso frontal
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,-2);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-0.3f,-size/2,20);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (-2,-size/2,20);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-2,-size/2,-2);//esquina superior izquierda 
         //piso lateral izquierdo
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,-2);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (-0.3f,-size/2,0);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (20,-size/2,0);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (20,-size/2,-2);//esquina superior izquierda 
        glEnd();
    }
    
     public void drawCancha(float size){
         
        glBegin (GL_QUADS);
        
        glTexCoord2d(0,0);glVertex3f (-16,-size/2,-3);//esquina inferior izquierda
        glTexCoord2d(0,30);glVertex3f (-16,-size/2,21);//esquina inferior derecha
        glTexCoord2d(30,30);glVertex3f (-4,-size/2,21);//esquina superior derecha
        glTexCoord2d(30,0);glVertex3f (-4,-size/2,-3);//esquina superior izquierda 
               
        glEnd();
        
    }
     
      public void drawLineas ()  {
          
        glBegin (GL_QUADS);
          
    //Lineas cancha
    
        glTexCoord2d(0,0);glVertex3f (-16,-0.495f,20.8f);//esquina inferior izquierda
        glTexCoord2d(0,10);glVertex3f (-16,-0.495f,21);//esquina inferior derecha
        glTexCoord2d(10,10);glVertex3f (-4,-0.495f,21);//esquina superior derecha
        glTexCoord2d(10,0);glVertex3f (-4,-0.495f,20.8f);//esquina superior izquierda
  
        glTexCoord2d(0,0);glVertex3f (-13.5f,-0.495f,16.3f);//esquina inferior izquierda
        glTexCoord2d(0,10);glVertex3f (-13.5f,-0.495f,16.5f);//esquina inferior derecha
        glTexCoord2d(10,10);glVertex3f (-6.5f,-0.495f,16.5f);//esquina superior derecha
        glTexCoord2d(10,0);glVertex3f (-6.5f,-0.495f,16.3f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-16,-0.495f,-2.8f);//esquina inferior izquierda
        glTexCoord2d(0,10);glVertex3f (-16,-0.495f,-3);//esquina inferior derecha
        glTexCoord2d(10,10);glVertex3f (-4,-0.495f,-3);//esquina superior derecha
        glTexCoord2d(10,0);glVertex3f (-4,-0.495f,-2.8f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-13.5f,-0.495f,1.7f);//esquina inferior izquierda
        glTexCoord2d(0,10);glVertex3f (-13.5f,-0.495f,1.5f);//esquina inferior derecha
        glTexCoord2d(10,10);glVertex3f (-6.5f,-0.495f,1.5f);//esquina superior derecha
        glTexCoord2d(10,0);glVertex3f (-6.5f,-0.495f,1.7f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-16,-0.495f,-3);//esquina inferior izquierda
        glTexCoord2d(0,10);glVertex3f (-16,-0.495f,21);//esquina inferior derecha
        glTexCoord2d(10,10);glVertex3f (-15.8f,-0.495f,21);//esquina superior derecha
        glTexCoord2d(10,0);glVertex3f (-15.8f,-0.495f,-3);//esquina superior izquierda
  
        glTexCoord2d(0,0);glVertex3f (-13.5f,-0.495f,-3);//esquina inferior izquierda
        glTexCoord2d(0,10);glVertex3f (-13.5f,-0.495f,1.7f);//esquina inferior derecha
        glTexCoord2d(10,10);glVertex3f (-13.3f,-0.495f,1.7f);//esquina superior derecha
        glTexCoord2d(10,0);glVertex3f (-13.3f,-0.495f,-3);//esquina superior izquierda        
        
        glTexCoord2d(0,0);glVertex3f (-13.5f,-0.495f,16.5f);//esquina inferior izquierda
        glTexCoord2d(0,10);glVertex3f (-13.5f,-0.495f,21);//esquina inferior derecha
        glTexCoord2d(10,10);glVertex3f (-13.3f,-0.495f,21);//esquina superior derecha
        glTexCoord2d(10,0);glVertex3f (-13.3f,-0.495f,16.5f);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-4.2f,-0.495f,-3);//esquina inferior izquierda
        glTexCoord2d(0,10);glVertex3f (-4.2f,-0.495f,21);//esquina inferior derecha
        glTexCoord2d(10,10);glVertex3f (-4,-0.495f,21);//esquina superior derecha
        glTexCoord2d(10,0);glVertex3f (-4,-0.495f,-3);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-6.5f,-0.495f,-3);//esquina inferior izquierda
        glTexCoord2d(0,10);glVertex3f (-6.5f,-0.495f,1.7f);//esquina inferior derecha
        glTexCoord2d(10,10);glVertex3f (-6.7f,-0.495f,1.7f);//esquina superior derecha
        glTexCoord2d(10,0);glVertex3f (-6.7f,-0.495f,-3);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-6.5f,-0.495f,16.5f);//esquina inferior izquierda
        glTexCoord2d(0,10);glVertex3f (-6.5f,-0.495f,21);//esquina inferior derecha
        glTexCoord2d(10,0);glVertex3f (-6.7f,-0.495f,21);//esquina superior derecha
        glTexCoord2d(10,0);glVertex3f (-6.7f,-0.495f,16.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-16,-0.495f,8.9f);//esquina inferior izquierda
        glTexCoord2d(0,10);glVertex3f (-16,-0.495f,9.1f);//esquina inferior derecha
        glTexCoord2d(10,10);glVertex3f (-4,-0.495f,9.1f);//esquina superior derecha
        glTexCoord2d(10,0);glVertex3f (-4,-0.495f,8.9f);//esquina superior izquierda
        
     
        glEnd();

      }
        
     
     
     public void drawExtCancha (float size){
        glBegin (GL_QUADS);
         //piso frontal
        glTexCoord2d(0,0);glVertex3f (-2,-size/2,-3);//esquina inferior izquierda
        glTexCoord2d(0,5);glVertex3f (-2,-size/2,21);//esquina inferior derecha
        glTexCoord2d(5,5);glVertex3f (-4,-size/2,21);//esquina superior derecha
        glTexCoord2d(5,0);glVertex3f (-4,-size/2,-3);//esquina superior izquierda 
        
        //piso lateral derecho
        glTexCoord2d(0,0);glVertex3f (-2,-size/2,21);//esquina inferior izquierda
        glTexCoord2d(0,5);glVertex3f (-2,-size/2,23);//esquina inferior derecha
        glTexCoord2d(5,5);glVertex3f (-16,-size/2,23);//esquina superior derecha
        glTexCoord2d(5,0);glVertex3f (-16,-size/2,21);//esquina superior izquierda 
        
        //piso lateral derecho
        glTexCoord2d(0,0);glVertex3f (-2,-size/2,-5);//esquina inferior izquierda
        glTexCoord2d(0,5);glVertex3f (-2,-size/2,-3);//esquina inferior derecha
        glTexCoord2d(5,5);glVertex3f (-16,-size/2,-3);//esquina superior derecha
        glTexCoord2d(5,0);glVertex3f (-16,-size/2,-5);//esquina superior izquierda 
    
         //piso frontal
        glTexCoord2d(0,0);glVertex3f (-16,-size/2,-5);//esquina inferior izquierda
        glTexCoord2d(0,5);glVertex3f (-16,-size/2,23);//esquina inferior derecha
        glTexCoord2d(5,5);glVertex3f (-18,-size/2,23);//esquina superior derecha
        glTexCoord2d(5,0);glVertex3f (-18,-size/2,-5);//esquina superior izquierda 
        
        glEnd();
     }
}
