
package vmorales;

import static org.lwjgl.opengl.GL11.*;


public class Paredes {
    //paredes externas
    public void drawParedLateral(float size){
        //usar primitivas para dibujar el cubo (tiene 6 lados)
        glBegin (GL_QUADS);
        
        //Cara frontal
        glTexCoord2d(0,0);glVertex3f (-size/2,-size/2,0.3f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (size/2,-size/2,0.3f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (size/2,1.5f,0.3f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (-size/2,1.5f,0.3f);//esquina superior izquierda 
        
       
        //Cara posterior 
        glTexCoord2d(0,0);glVertex3f (-size/2,-size/2,0);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (size/2,-size/2,0);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (size/2,1.5f,0);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (-size/2,1.5f,0);//esquina superior izquierda 
        
        
        //cara lateral derecha
        glTexCoord2d(0,0);glVertex3f (size/2,-size/2,0.3f);//esquina inferior frontal
        glTexCoord2d(0,2);glVertex3f (size/2,-size/2,0);//esquina inferior posterior
        glTexCoord2d(2,2);glVertex3f (size/2,1.5f,0);//esquina superior posterior
        glTexCoord2d(2,0);glVertex3f (size/2,1.5f,0.3f);//esquina superior frontal 
        
        
        //cara lateral izquierda
        glTexCoord2d(0,0);glVertex3f (-size/2,-size/2,0.3f);//esquina inferior frontal
        glTexCoord2d(0,2);glVertex3f (-size/2,-size/2,0);//esquina inferior posterior
        glTexCoord2d(2,2);glVertex3f (-size/2,1.5f,0);//esquina superior posterior
        glTexCoord2d(2,0);glVertex3f (-size/2,1.5f,0.3f);//esquina superior frontal 
        
        
        //cara superior
        glTexCoord2d(0,0);glVertex3f (-size/2,1.5f,0.3f);//esquina frontal izquierda
        glTexCoord2d(0,2);glVertex3f (size/2,1.5f,0.3f);//esquina frontal derecha
        glTexCoord2d(2,2);glVertex3f (size/2,1.5f,0);//esquina posterior derecha
        glTexCoord2d(2,0);glVertex3f (-size/2,1.5f,0);//esquina posterior izquierda 
        
        
        //cara inferior
        glTexCoord2d(0,0);glVertex3f (-size/2,-size/2,0.3f);//esquina frontal izquierda
        glTexCoord2d(0,2);glVertex3f (size/2,-size/2,0.3f);//esquina frontal derecha
        glTexCoord2d(2,2);glVertex3f (size/2,-size/2,0);//esquina posterior derecha
        glTexCoord2d(2,0);glVertex3f (-size/2,-size/2,0);//esquina posterior izquierda 
        glEnd();
    }

    public void drawParedLateralBordeF(float size){
        //usar primitivas para dibujar el cubo (tiene 6 lados)
        glBegin (GL_QUADS);
        
        //Cara frontal
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,0.3f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (size/2,-size/2,0.3f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (size/2,1.5f,0.3f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (-0.3f,1.5f,0.3f);//esquina superior izquierda 
        
       
        //Cara posterior 
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,0);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (size/2,-size/2,0);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (size/2,1.5f,0);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (-0.3f,1.5f,0);//esquina superior izquierda 
        
        
        //cara lateral derecha
        glTexCoord2d(0,0);glVertex3f (size/2,-size/2,0.3f);//esquina inferior frontal
        glTexCoord2d(0,2);glVertex3f (size/2,-size/2,0);//esquina inferior posterior
        glTexCoord2d(2,2);glVertex3f (size/2,1.5f,0);//esquina superior posterior
        glTexCoord2d(2,0);glVertex3f (size/2,1.5f,0.3f);//esquina superior frontal 
        
        
        //cara lateral izquierda
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,0.3f);//esquina inferior frontal
        glTexCoord2d(0,2);glVertex3f (-0.3f,-size/2,0);//esquina inferior posterior
        glTexCoord2d(2,2);glVertex3f (-0.3f,1.5f,0);//esquina superior posterior
        glTexCoord2d(2,0);glVertex3f (-0.3f,1.5f,0.3f);//esquina superior frontal 
        
        
        //cara superior
        glTexCoord2d(0,0);glVertex3f (-0.3f,1.5f,0.3f);//esquina frontal izquierda
        glTexCoord2d(0,2);glVertex3f (size/2,1.5f,0.3f);//esquina frontal derecha
        glTexCoord2d(2,2);glVertex3f (size/2,1.5f,0);//esquina posterior derecha
        glTexCoord2d(2,0);glVertex3f (-0.3f,1.5f,0);//esquina posterior izquierda 
        
        
        //cara inferior
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,0.3f);//esquina frontal izquierda
        glTexCoord2d(0,2);glVertex3f (size/2,-size/2,0.3f);//esquina frontal derecha
        glTexCoord2d(2,2);glVertex3f (size/2,-size/2,0);//esquina posterior derecha
        glTexCoord2d(2,0);glVertex3f (-0.3f,-size/2,0);//esquina posterior izquierda 
        glEnd();
    }

    public void drawParedLateralBordeP(float size){
        //usar primitivas para dibujar el cubo (tiene 6 lados)
        glBegin (GL_QUADS);
        
        //Cara frontal
        glTexCoord2d(0,0);glVertex3f (-size/2,-size/2,0.3f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0,-size/2,0.3f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0,1.5f,0.3f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (-size/2,1.5f,0.3f);//esquina superior izquierda 
        
       
        //Cara posterior 
        glTexCoord2d(0,0);glVertex3f (-size/2,-size/2,0);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0,-size/2,0);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0,1.5f,0);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (-size/2,1.5f,0);//esquina superior izquierda 
        
        
        //cara lateral derecha
        glTexCoord2d(0,0);glVertex3f (0,-size/2,0.3f);//esquina inferior frontal
        glTexCoord2d(0,2);glVertex3f (0,-size/2,0);//esquina inferior posterior
        glTexCoord2d(2,2);glVertex3f (0,1.5f,0);//esquina superior posterior
        glTexCoord2d(2,0);glVertex3f (0,1.5f,0.3f);//esquina superior frontal 
        
        
        //cara lateral izquierda
        glTexCoord2d(0,0);glVertex3f (-size/2,-size/2,0.3f);//esquina inferior frontal
        glTexCoord2d(0,2);glVertex3f (-size/2,-size/2,0);//esquina inferior posterior
        glTexCoord2d(2,2);glVertex3f (-size/2,1.5f,0);//esquina superior posterior
        glTexCoord2d(2,0);glVertex3f (-size/2,1.5f,0.3f);//esquina superior frontal 
        
        
        //cara superior
        glTexCoord2d(0,0);glVertex3f (-size/2,1.5f,0.3f);//esquina frontal izquierda
        glTexCoord2d(0,2);glVertex3f (0,1.5f,0.3f);//esquina frontal derecha
        glTexCoord2d(2,2);glVertex3f (0,1.5f,0);//esquina posterior derecha
        glTexCoord2d(2,0);glVertex3f (-size/2,1.5f,0);//esquina posterior izquierda 
        
        
        //cara inferior
        glTexCoord2d(0,0);glVertex3f (-size/2,-size/2,0.3f);//esquina frontal izquierda
        glTexCoord2d(0,2);glVertex3f (0,-size/2,0.3f);//esquina frontal derecha
        glTexCoord2d(2,2);glVertex3f (0,-size/2,0);//esquina posterior derecha
        glTexCoord2d(2,0);glVertex3f (-size/2,-size/2,0);//esquina posterior izquierda 
        glEnd();
    }
    public void drawParedFrontal(float size){
        //usar primitivas para dibujar el cubo (tiene 6 lados)
        glBegin (GL_QUADS);
        
        //Cara frontal
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,size/2);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0,-size/2,size/2);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0,1.5f,size/2);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (-0.3f,1.5f,size/2);//esquina superior izquierda 
        
       
        //Cara posterior 
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,-size/2);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0,-size/2,-size/2);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0,1.5f,-size/2);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (-0.3f,1.5f,-size/2);//esquina superior izquierda 
        
        
        //cara lateral derecha
        glTexCoord2d(0,0);glVertex3f (0,-size/2,size/2);//esquina inferior frontal
        glTexCoord2d(0,2);glVertex3f (0,-size/2,-size/2);//esquina inferior posterior
        glTexCoord2d(2,2);glVertex3f (0,1.5f,-size/2);//esquina superior posterior
        glTexCoord2d(2,0);glVertex3f (0,1.5f,size/2);//esquina superior frontal 
        
        
        //cara lateral izquierda
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,size/2);//esquina inferior frontal
        glTexCoord2d(0,2);glVertex3f (-0.3f,-size/2,-size/2);//esquina inferior posterior
        glTexCoord2d(2,2);glVertex3f (-0.3f,1.5f,-size/2);//esquina superior posterior
        glTexCoord2d(2,0);glVertex3f (-0.3f,1.5f,size/2);//esquina superior frontal 
        
        
        //cara superior
        glTexCoord2d(0,0);glVertex3f (-0.3f,1.5f,size/2);//esquina frontal izquierda
        glTexCoord2d(0,2);glVertex3f (0,1.5f,size/2);//esquina frontal derecha
        glTexCoord2d(2,2);glVertex3f (0,1.5f,-size/2);//esquina posterior derecha
        glTexCoord2d(2,0);glVertex3f (-0.3f,1.5f,-size/2);//esquina posterior izquierda 
        
        
        //cara inferior
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,size/2);//esquina frontal izquierda
        glTexCoord2d(0,2);glVertex3f (0,-size/2,size/2);//esquina frontal derecha
        glTexCoord2d(2,2);glVertex3f (0,-size/2,-size/2);//esquina posterior derecha
        glTexCoord2d(2,0);glVertex3f (-0.3f,-size/2,-size/2);//esquina posterior izquierda 
        glEnd();
    }
  
    
    public void drawParedFrontalBordeI(float size){
        //usar primitivas para dibujar el cubo (tiene 6 lados)
        glBegin (GL_QUADS);
        
        //Cara frontal
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,size/2);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0,-size/2,size/2);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0,1.5f,size/2);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (-0.3f,1.5f,size/2);//esquina superior izquierda 
        
       
        //Cara posterior 
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,-0.7f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0,-size/2,-0.7f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0,1.5f,-0.7f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (-0.3f,1.5f,-0.7f);//esquina superior izquierda 
        
        
        //cara lateral derecha
        glTexCoord2d(0,0);glVertex3f (0,-size/2,size/2);//esquina inferior frontal
        glTexCoord2d(0,2);glVertex3f (0,-size/2,-0.7f);//esquina inferior posterior
        glTexCoord2d(2,2);glVertex3f (0,1.5f,-0.7f);//esquina superior posterior
        glTexCoord2d(2,0);glVertex3f (0,1.5f,size/2);//esquina superior frontal 
        
        
        //cara lateral izquierda
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,size/2);//esquina inferior frontal
        glTexCoord2d(0,2);glVertex3f (-0.3f,-size/2,-0.7f);//esquina inferior posterior
        glTexCoord2d(2,2);glVertex3f (-0.3f,1.5f,-0.7f);//esquina superior posterior
        glTexCoord2d(2,0);glVertex3f (-0.3f,1.5f,size/2);//esquina superior frontal 
        
        
        //cara superior
        glTexCoord2d(0,0);glVertex3f (-0.3f,1.5f,size/2);//esquina frontal izquierda
        glTexCoord2d(0,2);glVertex3f (0,1.5f,size/2);//esquina frontal derecha
        glTexCoord2d(2,2);glVertex3f (0,1.5f,-0.7f);//esquina posterior derecha
        glTexCoord2d(2,0);glVertex3f (-0.3f,1.5f,-0.7f);//esquina posterior izquierda 
        
        
        //cara inferior
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,size/2);//esquina frontal izquierda
        glTexCoord2d(0,2);glVertex3f (0,-size/2,size/2);//esquina frontal derecha
        glTexCoord2d(2,2);glVertex3f (0,-size/2,-0.7f);//esquina posterior derecha
        glTexCoord2d(2,0);glVertex3f (-0.3f,-size/2,-0.7f);//esquina posterior izquierda 
        glEnd();
    }
    public void drawParedFrontalBordeD(float size){
        //usar primitivas para dibujar el cubo (tiene 6 lados)
        glBegin (GL_QUADS);
        
        //Cara frontal
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,1);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0,-size/2,1);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0,1.5f,1);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (-0.3f,1.5f,1);//esquina superior izquierda 
        
       
        //Cara posterior 
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,-size/2);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0,-size/2,-size/2);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0,1.5f,-size/2);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (-0.3f,1.5f,-size/2);//esquina superior izquierda 
        
        
        //cara lateral derecha
        glTexCoord2d(0,0);glVertex3f (0,-size/2,1);//esquina inferior frontal
        glTexCoord2d(0,2);glVertex3f (0,-size/2,-size/2);//esquina inferior posterior
        glTexCoord2d(2,2);glVertex3f (0,1.5f,-size/2);//esquina superior posterior
        glTexCoord2d(2,0);glVertex3f (0,1.5f,1);//esquina superior frontal 
        
        
        //cara lateral izquierda
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,1);//esquina inferior frontal
        glTexCoord2d(0,2);glVertex3f (-0.3f,-size/2,-size/2);//esquina inferior posterior
        glTexCoord2d(2,2);glVertex3f (-0.3f,1.5f,-size/2);//esquina superior posterior
        glTexCoord2d(2,0);glVertex3f (-0.3f,1.5f,1);//esquina superior frontal 
        
        
        //cara superior
        glTexCoord2d(0,0);glVertex3f (-0.3f,1.5f,1);//esquina frontal izquierda
        glTexCoord2d(0,2);glVertex3f (0,1.5f,1);//esquina frontal derecha
        glTexCoord2d(2,2);glVertex3f (0,1.5f,-size/2);//esquina posterior derecha
        glTexCoord2d(2,0);glVertex3f (-0.3f,1.5f,-size/2);//esquina posterior izquierda 
        
        
        //cara inferior
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,1);//esquina frontal izquierda
        glTexCoord2d(0,2);glVertex3f (0,-size/2,1);//esquina frontal derecha
        glTexCoord2d(2,2);glVertex3f (0,-size/2,-size/2);//esquina posterior derecha
        glTexCoord2d(2,0);glVertex3f (-0.3f,-size/2,-size/2);//esquina posterior izquierda 
        glEnd();
    }
    
    
    //PAREDES INTERNAS
    
    
      public void drawParedFrontalBaño(float size){
        //usar primitivas para dibujar el cubo (tiene 6 lados)
        glBegin (GL_QUADS);
        
        //Cara frontal
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,size/2);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (0,-size/2,size/2);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (0,1.5f,size/2);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-0.3f,1.5f,size/2);//esquina superior izquierda 
        
       
        //Cara posterior 
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,-size/2);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (0,-size/2,-size/2);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (0,1.5f,-size/2);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-0.3f,1.5f,-size/2);//esquina superior izquierda 
        
        
        //cara lateral derecha
        glTexCoord2d(0,0);glVertex3f (0,-size/2,size/2);//esquina inferior frontal
        glTexCoord2d(0,1);glVertex3f (0,-size/2,-size/2);//esquina inferior posterior
        glTexCoord2d(1,1);glVertex3f (0,1.5f,-size/2);//esquina superior posterior
        glTexCoord2d(1,0);glVertex3f (0,1.5f,size/2);//esquina superior frontal 
        
        
        //cara lateral izquierda
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,size/2);//esquina inferior frontal
        glTexCoord2d(0,1);glVertex3f (-0.3f,-size/2,-size/2);//esquina inferior posterior
        glTexCoord2d(1,1);glVertex3f (-0.3f,1.5f,-size/2);//esquina superior posterior
        glTexCoord2d(1,0);glVertex3f (-0.3f,1.5f,size/2);//esquina superior frontal 
        
        
        //cara superior
        glTexCoord2d(0,0);glVertex3f (-0.3f,1.5f,size/2);//esquina frontal izquierda
        glTexCoord2d(0,1);glVertex3f (0,1.5f,size/2);//esquina frontal derecha
        glTexCoord2d(1,1);glVertex3f (0,1.5f,-size/2);//esquina posterior derecha
        glTexCoord2d(1,0);glVertex3f (-0.3f,1.5f,-size/2);//esquina posterior izquierda 
        
        
        //cara inferior
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,size/2);//esquina frontal izquierda
        glTexCoord2d(0,1);glVertex3f (0,-size/2,size/2);//esquina frontal derecha
        glTexCoord2d(1,1);glVertex3f (0,-size/2,-size/2);//esquina posterior derecha
        glTexCoord2d(1,0);glVertex3f (-0.3f,-size/2,-size/2);//esquina posterior izquierda 
        glEnd();
    }
          public void drawParedLateralBaño(float size){
        //usar primitivas para dibujar el cubo (tiene 6 lados)
        glBegin (GL_QUADS);
        
        //Cara frontal
        glTexCoord2d(0,0);glVertex3f (-size/2,-size/2,0.3f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (size/2,-size/2,0.3f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (size/2,1.5f,0.3f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-size/2,1.5f,0.3f);//esquina superior izquierda 
        
       
        //Cara posterior 
        glTexCoord2d(0,0);glVertex3f (-size/2,-size/2,0);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (size/2,-size/2,0);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (size/2,1.5f,0);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-size/2,1.5f,0);//esquina superior izquierda 
        
        
        //cara lateral derecha
        glTexCoord2d(0,0);glVertex3f (size/2,-size/2,0.3f);//esquina inferior frontal
        glTexCoord2d(0,1);glVertex3f (size/2,-size/2,0);//esquina inferior posterior
        glTexCoord2d(1,1);glVertex3f (size/2,1.5f,0);//esquina superior posterior
        glTexCoord2d(1,0);glVertex3f (size/2,1.5f,0.3f);//esquina superior frontal 
        
        
        //cara lateral izquierda
        glTexCoord2d(0,0);glVertex3f (-size/2,-size/2,0.3f);//esquina inferior frontal
        glTexCoord2d(0,1);glVertex3f (-size/2,-size/2,0);//esquina inferior posterior
        glTexCoord2d(1,1);glVertex3f (-size/2,1.5f,0);//esquina superior posterior
        glTexCoord2d(1,0);glVertex3f (-size/2,1.5f,0.3f);//esquina superior frontal 
        
        
        //cara superior
        glTexCoord2d(0,0);glVertex3f (-size/2,1.5f,0.3f);//esquina frontal izquierda
        glTexCoord2d(0,1);glVertex3f (size/2,1.5f,0.3f);//esquina frontal derecha
        glTexCoord2d(1,1);glVertex3f (size/2,1.5f,0);//esquina posterior derecha
        glTexCoord2d(1,0);glVertex3f (-size/2,1.5f,0);//esquina posterior izquierda 
        
        
        //cara inferior
        glTexCoord2d(0,0);glVertex3f (-size/2,-size/2,0.3f);//esquina frontal izquierda
        glTexCoord2d(0,1);glVertex3f (size/2,-size/2,0.3f);//esquina frontal derecha
        glTexCoord2d(1,1);glVertex3f (size/2,-size/2,0);//esquina posterior derecha
        glTexCoord2d(1,0);glVertex3f (-size/2,-size/2,0);//esquina posterior izquierda 
        glEnd();
    }
          public void drawParedLateralBordePBaño(float size){
        //usar primitivas para dibujar el cubo (tiene 6 lados)
        glBegin (GL_QUADS);
        
        //Cara frontal
        glTexCoord2d(0,0);glVertex3f (-size/2,-size/2,0.3f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (0,-size/2,0.3f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (0,1.5f,0.3f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-size/2,1.5f,0.3f);//esquina superior izquierda 
        
       
        //Cara posterior 
        glTexCoord2d(0,0);glVertex3f (-size/2,-size/2,0);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (0,-size/2,0);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (0,1.5f,0);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-size/2,1.5f,0);//esquina superior izquierda 
        
        
        //cara lateral derecha
        glTexCoord2d(0,0);glVertex3f (0,-size/2,0.3f);//esquina inferior frontal
        glTexCoord2d(0,1);glVertex3f (0,-size/2,0);//esquina inferior posterior
        glTexCoord2d(1,1);glVertex3f (0,1.5f,0);//esquina superior posterior
        glTexCoord2d(1,0);glVertex3f (0,1.5f,0.3f);//esquina superior frontal 
        
        
        //cara lateral izquierda
        glTexCoord2d(0,0);glVertex3f (-size/2,-size/2,0.3f);//esquina inferior frontal
        glTexCoord2d(0,1);glVertex3f (-size/2,-size/2,0);//esquina inferior posterior
        glTexCoord2d(1,1);glVertex3f (-size/2,1.5f,0);//esquina superior posterior
        glTexCoord2d(1,0);glVertex3f (-size/2,1.5f,0.3f);//esquina superior frontal 
        
        
        //cara superior
        glTexCoord2d(0,0);glVertex3f (-size/2,1.5f,0.3f);//esquina frontal izquierda
        glTexCoord2d(0,1);glVertex3f (0,1.5f,0.3f);//esquina frontal derecha
        glTexCoord2d(1,1);glVertex3f (0,1.5f,0);//esquina posterior derecha
        glTexCoord2d(1,0);glVertex3f (-size/2,1.5f,0);//esquina posterior izquierda 
        
        
        //cara inferior
        glTexCoord2d(0,0);glVertex3f (-size/2,-size/2,0.3f);//esquina frontal izquierda
        glTexCoord2d(0,1);glVertex3f (0,-size/2,0.3f);//esquina frontal derecha
        glTexCoord2d(1,1);glVertex3f (0,-size/2,0);//esquina posterior derecha
        glTexCoord2d(1,0);glVertex3f (-size/2,-size/2,0);//esquina posterior izquierda 
        glEnd();
    }
          public void drawParedFrontalBordeIBaño(float size){
        //usar primitivas para dibujar el cubo (tiene 6 lados)
        glBegin (GL_QUADS);
        
        //Cara frontal
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,size/2);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (0,-size/2,size/2);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (0,1.5f,size/2);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-0.3f,1.5f,size/2);//esquina superior izquierda 
        
       
        //Cara posterior 
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,-0.7f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (0,-size/2,-0.7f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (0,1.5f,-0.7f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-0.3f,1.5f,-0.7f);//esquina superior izquierda 
        
        
        //cara lateral derecha
        glTexCoord2d(0,0);glVertex3f (0,-size/2,size/2);//esquina inferior frontal
        glTexCoord2d(0,1);glVertex3f (0,-size/2,-0.7f);//esquina inferior posterior
        glTexCoord2d(1,1);glVertex3f (0,1.5f,-0.7f);//esquina superior posterior
        glTexCoord2d(1,0);glVertex3f (0,1.5f,size/2);//esquina superior frontal 
        
        
        //cara lateral izquierda
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,size/2);//esquina inferior frontal
        glTexCoord2d(0,1);glVertex3f (-0.3f,-size/2,-0.7f);//esquina inferior posterior
        glTexCoord2d(1,1);glVertex3f (-0.3f,1.5f,-0.7f);//esquina superior posterior
        glTexCoord2d(1,0);glVertex3f (-0.3f,1.5f,size/2);//esquina superior frontal 
        
        
        //cara superior
        glTexCoord2d(0,0);glVertex3f (-0.3f,1.5f,size/2);//esquina frontal izquierda
        glTexCoord2d(0,1);glVertex3f (0,1.5f,size/2);//esquina frontal derecha
        glTexCoord2d(1,1);glVertex3f (0,1.5f,-0.7f);//esquina posterior derecha
        glTexCoord2d(1,0);glVertex3f (-0.3f,1.5f,-0.7f);//esquina posterior izquierda 
        
        
        //cara inferior
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,size/2);//esquina frontal izquierda
        glTexCoord2d(0,1);glVertex3f (0,-size/2,size/2);//esquina frontal derecha
        glTexCoord2d(1,1);glVertex3f (0,-size/2,-0.7f);//esquina posterior derecha
        glTexCoord2d(1,0);glVertex3f (-0.3f,-size/2,-0.7f);//esquina posterior izquierda 
        glEnd();
    }
    public void drawParedLateralBorde1(float size){
        //usar primitivas para dibujar el cubo (tiene 6 lados)
        glBegin (GL_QUADS);
        
        //Cara frontal
        glTexCoord2d(0,0);glVertex3f (-1,-size/2,0.3f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (size/2,-size/2,0.3f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (size/2,1.5f,0.3f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (-1,1.5f,0.3f);//esquina superior izquierda 
        
       
        //Cara posterior 
        glTexCoord2d(0,0);glVertex3f (-1,-size/2,0);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (size/2,-size/2,0);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (size/2,1.5f,0);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (-1,1.5f,0);//esquina superior izquierda 
        
        
        //cara lateral derecha
        glTexCoord2d(0,0);glVertex3f (size/2,-size/2,0.3f);//esquina inferior frontal
        glTexCoord2d(0,2);glVertex3f (size/2,-size/2,0);//esquina inferior posterior
        glTexCoord2d(2,2);glVertex3f (size/2,1.5f,0);//esquina superior posterior
        glTexCoord2d(2,0);glVertex3f (size/2,1.5f,0.3f);//esquina superior frontal 
        
        
        //cara lateral izquierda
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,0.3f);//esquina inferior frontal
        glTexCoord2d(0,2);glVertex3f (-0.3f,-size/2,0);//esquina inferior posterior
        glTexCoord2d(2,2);glVertex3f (-0.3f,1.5f,0);//esquina superior posterior
        glTexCoord2d(2,0);glVertex3f (-0.3f,1.5f,0.3f);//esquina superior frontal 
        
        
        //cara superior
        glTexCoord2d(0,0);glVertex3f (-1,1.5f,0.3f);//esquina frontal izquierda
        glTexCoord2d(0,2);glVertex3f (size/2,1.5f,0.3f);//esquina frontal derecha
        glTexCoord2d(2,2);glVertex3f (size/2,1.5f,0);//esquina posterior derecha
        glTexCoord2d(2,0);glVertex3f (-1,1.5f,0);//esquina posterior izquierda 
        
        
        //cara inferior
        glTexCoord2d(0,0);glVertex3f (-1,-size/2,0.3f);//esquina frontal izquierda
        glTexCoord2d(0,2);glVertex3f (size/2,-size/2,0.3f);//esquina frontal derecha
        glTexCoord2d(2,2);glVertex3f (size/2,-size/2,0);//esquina posterior derecha
        glTexCoord2d(2,0);glVertex3f (-1,-size/2,0);//esquina posterior izquierda 
        glEnd();
    }
    public void drawParedLateralBorde1Baño(float size){
        //usar primitivas para dibujar el cubo (tiene 6 lados)
        glBegin (GL_QUADS);
        
        //Cara frontal
        glTexCoord2d(0,0);glVertex3f (-1,-size/2,0.3f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (size/2,-size/2,0.3f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (size/2,1.5f,0.3f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-1,1.5f,0.3f);//esquina superior izquierda 
        
       
        //Cara posterior 
        glTexCoord2d(0,0);glVertex3f (-1,-size/2,0);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (size/2,-size/2,0);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (size/2,1.5f,0);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-1,1.5f,0);//esquina superior izquierda 
        
        
        //cara lateral derecha
        glTexCoord2d(0,0);glVertex3f (size/2,-size/2,0.3f);//esquina inferior frontal
        glTexCoord2d(0,1);glVertex3f (size/2,-size/2,0);//esquina inferior posterior
        glTexCoord2d(1,1);glVertex3f (size/2,1.5f,0);//esquina superior posterior
        glTexCoord2d(1,0);glVertex3f (size/2,1.5f,0.3f);//esquina superior frontal 
        
        
        //cara lateral izquierda
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,0.3f);//esquina inferior frontal
        glTexCoord2d(0,1);glVertex3f (-0.3f,-size/2,0);//esquina inferior posterior
        glTexCoord2d(1,1);glVertex3f (-0.3f,1.5f,0);//esquina superior posterior
        glTexCoord2d(1,0);glVertex3f (-0.3f,1.5f,0.3f);//esquina superior frontal 
        
        
        //cara superior
        glTexCoord2d(0,0);glVertex3f (-1,1.5f,0.3f);//esquina frontal izquierda
        glTexCoord2d(0,1);glVertex3f (size/2,1.5f,0.3f);//esquina frontal derecha
        glTexCoord2d(1,1);glVertex3f (size/2,1.5f,0);//esquina posterior derecha
        glTexCoord2d(1,0);glVertex3f (-1,1.5f,0);//esquina posterior izquierda 
        
        
        //cara inferior
        glTexCoord2d(0,0);glVertex3f (-1,-size/2,0.3f);//esquina frontal izquierda
        glTexCoord2d(0,1);glVertex3f (size/2,-size/2,0.3f);//esquina frontal derecha
        glTexCoord2d(1,1);glVertex3f (size/2,-size/2,0);//esquina posterior derecha
        glTexCoord2d(1,0);glVertex3f (-1,-size/2,0);//esquina posterior izquierda 
        glEnd();
    }
      public void drawParedLateralBorde2(float size){
        //usar primitivas para dibujar el cubo (tiene 6 lados)
        glBegin (GL_QUADS);
               
        //Cara frontal
        glTexCoord2d(0,0);glVertex3f (-size/2,-size/2,0.3f);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (1,-size/2,0.3f);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (1,1.5f,0.3f);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (-size/2,1.5f,0.3f);//esquina superior izquierda 
        
       
        //Cara posterior 
        glTexCoord2d(0,0);glVertex3f (-size/2,-size/2,0);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (1,-size/2,0);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (1,1.5f,0);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (-size/2,1.5f,0);//esquina superior izquierda 
        
        
        //cara lateral derecha
        glTexCoord2d(0,0);glVertex3f (1,-size/2,0.3f);//esquina inferior frontal
        glTexCoord2d(0,2);glVertex3f (1,-size/2,0);//esquina inferior posterior
        glTexCoord2d(2,2);glVertex3f (1,1.5f,0);//esquina superior posterior
        glTexCoord2d(2,0);glVertex3f (1,1.5f,0.3f);//esquina superior frontal 
        
        
        //cara lateral izquierda
        glTexCoord2d(0,0);glVertex3f (-size/2,-size/2,0.3f);//esquina inferior frontal
        glTexCoord2d(0,2);glVertex3f (-size/2,-size/2,0);//esquina inferior posterior
        glTexCoord2d(2,2);glVertex3f (-size/2,1.5f,0);//esquina superior posterior
        glTexCoord2d(2,0);glVertex3f (-size/2,1.5f,0.3f);//esquina superior frontal 
        
        
        //cara superior
        glTexCoord2d(0,0);glVertex3f (-size/2,1.5f,0.3f);//esquina frontal izquierda
        glTexCoord2d(0,2);glVertex3f (1,1.5f,0.3f);//esquina frontal derecha
        glTexCoord2d(2,2);glVertex3f (1,1.5f,0);//esquina posterior derecha
        glTexCoord2d(2,0);glVertex3f (-size/2,1.5f,0);//esquina posterior izquierda 
        
        
        //cara inferior
        glTexCoord2d(0,0);glVertex3f (-size/2,-size/2,0.3f);//esquina frontal izquierda
        glTexCoord2d(0,2);glVertex3f (1,-size/2,0.3f);//esquina frontal derecha
        glTexCoord2d(2,2);glVertex3f (1,-size/2,0);//esquina posterior derecha
        glTexCoord2d(2,0);glVertex3f (-size/2,-size/2,0);//esquina posterior izquierda 
        glEnd();
    }
      public void drawParedLateralBorde2Baño(float size){
        //usar primitivas para dibujar el cubo (tiene 6 lados)
        glBegin (GL_QUADS);
               
        //Cara frontal
        glTexCoord2d(0,0);glVertex3f (-size/2,-size/2,0.3f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (1,-size/2,0.3f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (1,1.5f,0.3f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-size/2,1.5f,0.3f);//esquina superior izquierda 
        
       
        //Cara posterior 
        glTexCoord2d(0,0);glVertex3f (-size/2,-size/2,0);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (1,-size/2,0);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (1,1.5f,0);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (-size/2,1.5f,0);//esquina superior izquierda 
        
        
        //cara lateral derecha
        glTexCoord2d(0,0);glVertex3f (1,-size/2,0.3f);//esquina inferior frontal
        glTexCoord2d(0,1);glVertex3f (1,-size/2,0);//esquina inferior posterior
        glTexCoord2d(1,1);glVertex3f (1,1.5f,0);//esquina superior posterior
        glTexCoord2d(1,0);glVertex3f (1,1.5f,0.3f);//esquina superior frontal 
        
        
        //cara lateral izquierda
        glTexCoord2d(0,0);glVertex3f (-size/2,-size/2,0.3f);//esquina inferior frontal
        glTexCoord2d(0,1);glVertex3f (-size/2,-size/2,0);//esquina inferior posterior
        glTexCoord2d(1,1);glVertex3f (-size/2,1.5f,0);//esquina superior posterior
        glTexCoord2d(1,0);glVertex3f (-size/2,1.5f,0.3f);//esquina superior frontal 
        
        
        //cara superior
        glTexCoord2d(0,0);glVertex3f (-size/2,1.5f,0.3f);//esquina frontal izquierda
        glTexCoord2d(0,1);glVertex3f (1,1.5f,0.3f);//esquina frontal derecha
        glTexCoord2d(1,1);glVertex3f (1,1.5f,0);//esquina posterior derecha
        glTexCoord2d(1,0);glVertex3f (-size/2,1.5f,0);//esquina posterior izquierda 
        
        
        //cara inferior
        glTexCoord2d(0,0);glVertex3f (-size/2,-size/2,0.3f);//esquina frontal izquierda
        glTexCoord2d(0,1);glVertex3f (1,-size/2,0.3f);//esquina frontal derecha
        glTexCoord2d(1,1);glVertex3f (1,-size/2,0);//esquina posterior derecha
        glTexCoord2d(1,0);glVertex3f (-size/2,-size/2,0);//esquina posterior izquierda 
        glEnd();
    }
      public void drawParedFrontalBorde1(float size){
        //usar primitivas para dibujar el cubo (tiene 6 lados)
        glBegin (GL_QUADS);
        
        //Cara frontal
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,size/2);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0,-size/2,size/2);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0,1.5f,size/2);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (-0.3f,1.5f,size/2);//esquina superior izquierda 
        
       
        //Cara posterior 
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,-1);//esquina inferior izquierda
        glTexCoord2d(0,2);glVertex3f (0,-size/2,-1);//esquina inferior derecha
        glTexCoord2d(2,2);glVertex3f (0,1.5f,-1);//esquina superior derecha
        glTexCoord2d(2,0);glVertex3f (-0.3f,1.5f,-1);//esquina superior izquierda 
        
        
        //cara lateral derecha
        glTexCoord2d(0,0);glVertex3f (0,-size/2,size/2);//esquina inferior frontal
        glTexCoord2d(0,2);glVertex3f (0,-size/2,-1);//esquina inferior posterior
        glTexCoord2d(2,2);glVertex3f (0,1.5f,-1);//esquina superior posterior
        glTexCoord2d(2,0);glVertex3f (0,1.5f,size/2);//esquina superior frontal 
        
        
        //cara lateral izquierda
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,size/2);//esquina inferior frontal
        glTexCoord2d(0,2);glVertex3f (-0.3f,-size/2,-1);//esquina inferior posterior
        glTexCoord2d(2,2);glVertex3f (-0.3f,1.5f,-1);//esquina superior posterior
        glTexCoord2d(2,0);glVertex3f (-0.3f,1.5f,size/2);//esquina superior frontal 
        
        
        //cara superior
        glTexCoord2d(0,0);glVertex3f (-0.3f,1.5f,size/2);//esquina frontal izquierda
        glTexCoord2d(0,2);glVertex3f (0,1.5f,size/2);//esquina frontal derecha
        glTexCoord2d(2,2);glVertex3f (0,1.5f,-1);//esquina posterior derecha
        glTexCoord2d(2,0);glVertex3f (-0.3f,1.5f,-1);//esquina posterior izquierda 
        
        
        //cara inferior
        glTexCoord2d(0,0);glVertex3f (-0.3f,-size/2,size/2);//esquina frontal izquierda
        glTexCoord2d(0,2);glVertex3f (0,-size/2,size/2);//esquina frontal derecha
        glTexCoord2d(2,2);glVertex3f (0,-size/2,-1);//esquina posterior derecha
        glTexCoord2d(2,0);glVertex3f (-0.3f,-size/2,-1);//esquina posterior izquierda 
        glEnd();
    }
}
