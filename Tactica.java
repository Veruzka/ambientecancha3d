
package vmorales;

import static org.lwjgl.opengl.GL11.GL_POLYGON;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glTexCoord2d;
import static org.lwjgl.opengl.GL11.glVertex3f;
import static vmorales.Rehabilitacion.Pi;


public class Tactica {
    
    public void drawPizarra(){
        
        glBegin (GL_QUADS);
          
        glTexCoord2d(0,0);glVertex3f (12.65f,0f,7.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (12.65f,0f,9.8f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (12.65f,1f,9.8f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (12.65f,1f,7.5f);//esquina superior izquierda
        
         glEnd();
    }
    
 
}
