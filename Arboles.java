
package vmorales;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glTexCoord2d;
import static org.lwjgl.opengl.GL11.glVertex3f;

public class Arboles {
    
    
    public void drawArbol1(){
        
        glBegin (GL_QUADS);
        
      //Base Arbol1
        
        glTexCoord2d(0,0);glVertex3f (20f,0.5f,15.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (20f,0.5f,12.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21f,1.5f,12.9f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (21f,1.5f,15.2f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (23f,0.5f,15.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (23f,0.5f,12.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,1.5f,12.9f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22f,1.5f,15.2f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (20f,0.5f,15.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21f,1.5f,15.2f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,1.5f,15.2f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (23f,0.5f,15.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (20f,0.5f,12.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21f,1.5f,12.9f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,1.5f,12.9f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (23f,0.5f,12.5f);//esquina superior izquierda
        
        
        //Base Arbol2
        
        glTexCoord2d(0,0);glVertex3f (20f,0.5f,11.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (20f,0.5f,8.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21f,1.5f,8.9f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (21f,1.5f,11.2f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (23f,0.5f,11.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (23f,0.5f,8.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,1.5f,8.9f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22f,1.5f,11.2f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (20f,0.5f,11.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21f,1.5f,11.2f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,1.5f,11.2f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (23f,0.5f,11.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (20f,0.5f,8.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21f,1.5f,8.9f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,1.5f,8.9f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (23f,0.5f,8.5f);//esquina superior izquierda
        
        //Base Arbol3
        
        glTexCoord2d(0,0);glVertex3f (20f,0.5f,7.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (20f,0.5f,4.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21f,1.5f,4.9f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (21f,1.5f,7.2f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (23f,0.5f,7.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (23f,0.5f,4.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,1.5f,4.9f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22f,1.5f,7.2f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (20f,0.5f,7.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21f,1.5f,7.2f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,1.5f,7.2f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (23f,0.5f,7.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (20f,0.5f,4.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21f,1.5f,4.9f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,1.5f,4.9f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (23f,0.5f,4.5f);//esquina superior izquierda
        
        //Base Arbol4
        
        glTexCoord2d(0,0);glVertex3f (20f,0.5f,3.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (20f,0.5f,0.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21f,1.5f,0.9f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (21f,1.5f,3.2f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (23f,0.5f,3.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (23f,0.5f,0.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,1.5f,0.9f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22f,1.5f,3.2f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (20f,0.5f,3.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21f,1.5f,3.2f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,1.5f,3.2f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (23f,0.5f,3.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (20f,0.5f,0.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21f,1.5f,0.9f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,1.5f,0.9f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (23f,0.5f,0.5f);//esquina superior izquierda
        
        
        //cima Arbol1
        
        glTexCoord2d(0,0);glVertex3f (20.6f,2.5f,15.1f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (20.6f,2.5f,12.9f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21.2f,3.5f,13.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (21.2f,3.5f,14.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (22.4f,2.5f,15.1f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (22.4f,2.5f,12.9f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21.8f,3.5f,13.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (21.8f,3.5f,14.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (20.6f,2.5f,15.1f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21.2f,3.5f,14.6f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21.8f,3.5f,14.6f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22.4f,2.5f,15.1f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (20.6f,2.5f,12.9f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21.2f,3.5f,13.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21.8f,3.5f,13.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22.4f,2.5f,12.9f);//esquina superior izquierda
        
        //cima Arbol2
        glTexCoord2d(0,0);glVertex3f (20.6f,2.5f,11.1f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (20.6f,2.5f,8.9f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21.2f,3.5f,9.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (21.2f,3.5f,10.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (22.4f,2.5f,11.1f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (22.4f,2.5f,8.9f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21.8f,3.5f,9.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (21.8f,3.5f,10.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (20.6f,2.5f,11.1f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21.2f,3.5f,10.6f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21.8f,3.5f,10.6f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22.4f,2.5f,11.1f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (20.6f,2.5f,8.9f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21.2f,3.5f,9.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21.8f,3.5f,9.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22.4f,2.5f,8.9f);//esquina superior izquierda
        
        //cima Arbol3
        glTexCoord2d(0,0);glVertex3f (20.6f,2.5f,7.1f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (20.6f,2.5f,4.9f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21.2f,3.5f,5.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (21.2f,3.5f,6.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (22.4f,2.5f,7.1f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (22.4f,2.5f,4.9f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21.8f,3.5f,5.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (21.8f,3.5f,6.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (20.6f,2.5f,7.1f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21.2f,3.5f,6.6f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21.8f,3.5f,6.6f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22.4f,2.5f,7.1f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (20.6f,2.5f,4.9f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21.2f,3.5f,5.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21.8f,3.5f,5.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22.4f,2.5f,4.9f);//esquina superior izquierda
        
        //cima Arbol4
        glTexCoord2d(0,0);glVertex3f (20.6f,2.5f,3.1f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (20.6f,2.5f,0.9f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21.2f,3.5f,1.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (21.2f,3.5f,2.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (22.4f,2.5f,3.1f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (22.4f,2.5f,0.9f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21.8f,3.5f,1.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (21.8f,3.5f,2.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (20.6f,2.5f,3.1f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21.2f,3.5f,2.6f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21.8f,3.5f,2.6f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22.4f,2.5f,3.1f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (20.6f,2.5f,0.9f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21.2f,3.5f,1.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21.8f,3.5f,1.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22.4f,2.5f,0.9f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (21.2f,3.5f,2.6f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21.2f,3.5f,1.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21.8f,3.5f,1.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (21.8f,3.5f,2.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (21.2f,3.5f,6.6f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21.2f,3.5f,5.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21.8f,3.5f,5.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (21.8f,3.5f,6.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (21.2f,3.5f,10.6f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21.2f,3.5f,9.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21.8f,3.5f,9.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (21.8f,3.5f,10.6f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (21.2f,3.5f,14.6f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21.2f,3.5f,13.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21.8f,3.5f,13.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (21.8f,3.5f,14.6f);//esquina superior izquierda
        
        glEnd();
    }
    
    public void drawArbol2(){
        
        glBegin (GL_QUADS);
        //Base Arbol1
        
        glTexCoord2d(0,0);glVertex3f (20.3f,1.5f,15.3f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (20.3f,1.5f,12.7f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21f,2.5f,13.3f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (21f,2.5f,14.8f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (22.7f,1.5f,15.3f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (22.7f,1.5f,12.7f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,2.5f,13.3f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22f,2.5f,14.8f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (20.3f,1.5f,15.3f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21f,2.5f,14.8f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,2.5f,14.8f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22.7f,1.5f,15.3f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (20.3f,1.5f,12.7f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21f,2.5f,13.3f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,2.5f,13.3f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22.7f,1.5f,12.7f);//esquina superior izquierda
        
        
        //Base Arbol2
        glTexCoord2d(0,0);glVertex3f (20.3f,1.5f,11.3f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (20.3f,1.5f,8.7f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21f,2.5f,9.3f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (21f,2.5f,10.8f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (22.7f,1.5f,11.3f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (22.7f,1.5f,8.7f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,2.5f,9.3f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22f,2.5f,10.8f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (20.3f,1.5f,11.3f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21f,2.5f,10.8f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,2.5f,10.8f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22.7f,1.5f,11.3f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (20.3f,1.5f,8.7f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21f,2.5f,9.3f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,2.5f,9.3f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22.7f,1.5f,8.7f);//esquina superior izquierda

        
        //Base Arbol3
        glTexCoord2d(0,0);glVertex3f (20.3f,1.5f,7.3f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (20.3f,1.5f,4.7f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21f,2.5f,5.3f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (21f,2.5f,6.8f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (22.7f,1.5f,7.3f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (22.7f,1.5f,4.7f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,2.5f,5.3f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22f,2.5f,6.8f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (20.3f,1.5f,7.3f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21f,2.5f,6.8f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,2.5f,6.8f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22.7f,1.5f,7.3f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (20.3f,1.5f,4.7f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21f,2.5f,5.3f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,2.5f,5.3f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22.7f,1.5f,4.7f);//esquina superior izquierda
    
        
        //Base Arbol4
        glTexCoord2d(0,0);glVertex3f (20.3f,1.5f,3.3f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (20.3f,1.5f,0.7f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21f,2.5f,1.3f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (21f,2.5f,2.8f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (22.7f,1.5f,3.3f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (22.7f,1.5f,0.7f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,2.5f,1.3f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22f,2.5f,2.8f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (20.3f,1.5f,3.3f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21f,2.5f,2.8f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,2.5f,2.8f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22.7f,1.5f,3.3f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (20.3f,1.5f,0.7f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21f,2.5f,1.3f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,2.5f,1.3f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22.7f,1.5f,0.7f);//esquina superior izquierda
       
      
        
        glEnd();
    }
    public void drawBase(){
        
        glBegin (GL_QUADS);
        
        glTexCoord2d(0,0);glVertex3f (19f,-0.45f,17.5f);//esquina inferior izquierda
        glTexCoord2d(0,4);glVertex3f (19f,-0.45f,0f);//esquina inferior derecha
        glTexCoord2d(4,4);glVertex3f (24f,-0.45f,0f);//esquina superior derecha
        glTexCoord2d(4,0);glVertex3f (24f,-0.45f,17.5f);//esquina superior izquierda  
        
        glEnd();
    }
    
    public void drawTroncos(){
        
        glBegin (GL_QUADS);
        
        //tronco1
        
        glTexCoord2d(0,0);glVertex3f (21.5f,0.6f,2.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21.5f,0.6f,1.7f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21.5f,-0.5f,1.7f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (21.5f,-0.5f,2.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (22f,0.6f,2.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (22f,0.6f,1.7f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,-0.5f,1.7f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22f,-0.5f,2.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (21.5f,-0.5f,2.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21.5f,0.6f,2.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,0.6f,2.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22f,-0.5f,2.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (21.5f,-0.5f,1.7f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21.5f,0.6f,1.7f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,0.6f,1.7f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22f,-0.5f,1.7f);//esquina superior izquierda
        
        //tronco2
        
        glTexCoord2d(0,0);glVertex3f (21.5f,0.6f,6.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21.5f,0.6f,5.7f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21.5f,-0.5f,5.7f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (21.5f,-0.5f,6.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (22f,0.6f,6.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (22f,0.6f,5.7f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,-0.5f,5.7f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22f,-0.5f,6.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (21.5f,-0.5f,6.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21.5f,0.6f,6.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,0.6f,6.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22f,-0.5f,6.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (21.5f,-0.5f,5.7f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21.5f,0.6f,5.7f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,0.6f,5.7f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22f,-0.5f,5.7f);//esquina superior izquierda
        
        //tronco3
        
        glTexCoord2d(0,0);glVertex3f (21.5f,0.6f,10.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21.5f,0.6f,9.7f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21.5f,-0.5f,9.7f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (21.5f,-0.5f,10.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (22f,0.6f,10.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (22f,0.6f,9.7f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,-0.5f,9.7f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22f,-0.5f,10.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (21.5f,-0.5f,10.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21.5f,0.6f,10.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,0.6f,10.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22f,-0.5f,10.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (21.5f,-0.5f,9.7f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21.5f,0.6f,9.7f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,0.6f,9.7f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22f,-0.5f,9.7f);//esquina superior izquierda
        
        //tronco4
        
        glTexCoord2d(0,0);glVertex3f (21.5f,0.6f,14.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21.5f,0.6f,13.7f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (21.5f,-0.5f,13.7f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (21.5f,-0.5f,14.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (22f,0.6f,14.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (22f,0.6f,13.7f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,-0.5f,13.7f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22f,-0.5f,14.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (21.5f,-0.5f,14.5f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21.5f,0.6f,14.5f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,0.6f,14.5f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22f,-0.5f,14.5f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (21.5f,-0.5f,13.7f);//esquina inferior izquierda
        glTexCoord2d(0,1);glVertex3f (21.5f,0.6f,13.7f);//esquina inferior derecha
        glTexCoord2d(1,1);glVertex3f (22f,0.6f,13.7f);//esquina superior derecha
        glTexCoord2d(1,0);glVertex3f (22f,-0.5f,13.7f);//esquina superior izquierda

        
        glEnd();
    }
}
