
package vmorales;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glNormal3f;
import static org.lwjgl.opengl.GL11.glTexCoord2d;
import static org.lwjgl.opengl.GL11.glVertex3f;

public class Tribuna {
    
    public void drawGradas (){
        
        glBegin (GL_QUADS);
        
        glTexCoord2d(0,0);glVertex3f (-18,-0.5f,-3);//esquina inferior izquierda
        glTexCoord2d(0,10);glVertex3f (-18,-0.5f,21);//esquina inferior derecha
        glTexCoord2d(10,10);glVertex3f (-18,0f,21);//esquina superior derecha
        glTexCoord2d(10,0);glVertex3f (-18,0f,-3);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-18,0f,-3);//esquina inferior izquierda
        glTexCoord2d(0,10);glVertex3f (-18,0f,21);//esquina inferior derecha
        glTexCoord2d(10,10);glVertex3f (-18.5f,0f,21);//esquina superior derecha
        glTexCoord2d(10,0);glVertex3f (-18.5f,0f,-3);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-18,-0.5f,-3);//esquina inferior izquierda
        glTexCoord2d(0,10);glVertex3f (-18.5f,-0.5f,-3);//esquina inferior derecha
        glTexCoord2d(10,10);glVertex3f (-18.5f,0f,-3);//esquina superior derecha
        glTexCoord2d(10,0);glVertex3f (-18f,0f,-3);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-18,-0.5f,21);//esquina inferior izquierda
        glTexCoord2d(0,10);glVertex3f (-18.5f,-0.5f,21);//esquina inferior derecha
        glTexCoord2d(10,10);glVertex3f (-18.5f,0f,21);//esquina superior derecha
        glTexCoord2d(10,0);glVertex3f (-18f,0f,21);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-18.5f,-0.5f,-3);//esquina inferior izquierda
        glTexCoord2d(0,10);glVertex3f (-19f,-0.5f,-3);//esquina inferior derecha
        glTexCoord2d(10,10);glVertex3f (-19f,0.5f,-3);//esquina superior derecha
        glTexCoord2d(10,0);glVertex3f (-18.5f,0.5f,-3);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-18.5f,-0.5f,21);//esquina inferior izquierda
        glTexCoord2d(0,10);glVertex3f (-19f,-0.5f,21);//esquina inferior derecha
        glTexCoord2d(10,10);glVertex3f (-19f,0.5f,21);//esquina superior derecha
        glTexCoord2d(10,0);glVertex3f (-18.5f,0.5f,21);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-19f,-0.5f,-3);//esquina inferior izquierda
        glTexCoord2d(0,10);glVertex3f (-19.5f,-0.5f,-3);//esquina inferior derecha
        glTexCoord2d(10,10);glVertex3f (-19.5f,1f,-3);//esquina superior derecha
        glTexCoord2d(10,0);glVertex3f (-19f,1f,-3);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-19f,-0.5f,21);//esquina inferior izquierda
        glTexCoord2d(0,10);glVertex3f (-19.5f,-0.5f,21);//esquina inferior derecha
        glTexCoord2d(10,10);glVertex3f (-19.5f,1f,21);//esquina superior derecha
        glTexCoord2d(10,0);glVertex3f (-19f,1f,21);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-18.5f,0f,-3);//esquina inferior izquierda
        glTexCoord2d(0,10);glVertex3f (-18.5f,0f,21);//esquina inferior derecha
        glTexCoord2d(10,10);glVertex3f (-18.5f,0.5f,21);//esquina superior derecha
        glTexCoord2d(10,0);glVertex3f (-18.5f,0.5f,-3);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-18.5f,0.5f,-3);//esquina inferior izquierda
        glTexCoord2d(0,30);glVertex3f (-18.5f,0.5f,21);//esquina inferior derecha
        glTexCoord2d(30,30);glVertex3f (-19f,0.5f,21);//esquina superior derecha
        glTexCoord2d(30,0);glVertex3f (-19f,0.5f,-3);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-19f,0.5f,-3);//esquina inferior izquierda
        glTexCoord2d(0,10);glVertex3f (-19f,0.5f,21);//esquina inferior derecha
        glTexCoord2d(10,10);glVertex3f (-19f,1f,21);//esquina superior derecha
        glTexCoord2d(10,0);glVertex3f (-19f,1f,-3);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-19f,1f,-3);//esquina inferior izquierda
        glTexCoord2d(0,10);glVertex3f (-19f,1f,21);//esquina inferior derecha
        glTexCoord2d(10,10);glVertex3f (-19.5f,1f,21);//esquina superior derecha
        glTexCoord2d(10,0);glVertex3f (-19.5f,1f,-3);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-19.5f,-0.5f,-3);//esquina inferior izquierda
        glTexCoord2d(0,10);glVertex3f (-19.5f,-0.5f,21);//esquina inferior derecha
        glTexCoord2d(10,10);glVertex3f (-19.5f,1f,21);//esquina superior derecha
        glTexCoord2d(10,0);glVertex3f (-19.5f,1f,-3);//esquina superior izquierda 
        
        glEnd();
    }
        public void drawVigas (){
            
        glBegin (GL_QUADS);
        
        //Vigas delanteras
        
        glTexCoord2d(0,0);glVertex3f (-18.2f,-0.5f,-3);//esquina inferior izquierda
        glTexCoord2d(0,30);glVertex3f (-18.2f,-0.5f,-3.15f);//esquina inferior derecha
        glTexCoord2d(30,30);glVertex3f (-18.2f,1.8f,-3.15f);//esquina superior derecha
        glTexCoord2d(30,0);glVertex3f (-18.2f,1.8f,-3);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-18.2f,-0.5f,21);//esquina inferior izquierda
        glTexCoord2d(0,30);glVertex3f (-18.2f,-0.5f,21.15f);//esquina inferior derecha
        glTexCoord2d(30,30);glVertex3f (-18.2f,1.8f,21.15f);//esquina superior derecha
        glTexCoord2d(30,0);glVertex3f (-18.2f,1.8f,21);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-18f,-0.5f,-3);//esquina inferior izquierda
        glTexCoord2d(0,30);glVertex3f (-18f,-0.5f,-3.15f);//esquina inferior derecha
        glTexCoord2d(30,30);glVertex3f (-18f,1.8f,-3.15f);//esquina superior derecha
        glTexCoord2d(30,0);glVertex3f (-18f,1.8f,-3);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-18f,-0.5f,21);//esquina inferior izquierda
        glTexCoord2d(0,30);glVertex3f (-18f,-0.5f,21.15f);//esquina inferior derecha
        glTexCoord2d(30,30);glVertex3f (-18f,1.8f,21.15f);//esquina superior derecha
        glTexCoord2d(30,0);glVertex3f (-18f,1.8f,21);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-18f,-0.5f,-3.15f);//esquina inferior izquierda
        glTexCoord2d(0,30);glVertex3f (-18.2f,-0.5f,-3.15f);//esquina inferior derecha
        glTexCoord2d(30,30);glVertex3f (-18.2f,1.8f,-3.15f);//esquina superior derecha
        glTexCoord2d(30,0);glVertex3f (-18f,1.8f,-3.15f);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-18f,-0.5f,21.15f);//esquina inferior izquierda
        glTexCoord2d(0,30);glVertex3f (-18.2f,-0.5f,21.15f);//esquina inferior derecha
        glTexCoord2d(30,30);glVertex3f (-18.2f,1.8f,21.15f);//esquina superior derecha
        glTexCoord2d(30,0);glVertex3f (-18f,1.8f,21.15f);//esquina superior izquierda
        
        
        //Vigas Traseras
        
        glTexCoord2d(0,0);glVertex3f (-19.5f,-0.5f,-3);//esquina inferior izquierda
        glTexCoord2d(0,30);glVertex3f (-19.5f,-0.5f,-3.15f);//esquina inferior derecha
        glTexCoord2d(30,30);glVertex3f (-19.5f,2.2f,-3.15f);//esquina superior derecha
        glTexCoord2d(30,0);glVertex3f (-19.5f,2.2f,-3);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-19.5f,-0.5f,21);//esquina inferior izquierda
        glTexCoord2d(0,30);glVertex3f (-19.5f,-0.5f,21.15f);//esquina inferior derecha
        glTexCoord2d(30,30);glVertex3f (-19.5f,2.2f,21.15f);//esquina superior derecha
        glTexCoord2d(30,0);glVertex3f (-19.5f,2.2f,21);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-19.3f,-0.5f,-3);//esquina inferior izquierda
        glTexCoord2d(0,30);glVertex3f (-19.3f,-0.5f,-3.15f);//esquina inferior derecha
        glTexCoord2d(30,30);glVertex3f (-19.3f,2.2f,-3.15f);//esquina superior derecha
        glTexCoord2d(30,0);glVertex3f (-19.3f,2.2f,-3);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-19.3f,-0.5f,21);//esquina inferior izquierda
        glTexCoord2d(0,30);glVertex3f (-19.3f,-0.5f,21.15f);//esquina inferior derecha
        glTexCoord2d(30,30);glVertex3f (-19.3f,2.2f,21.15f);//esquina superior derecha
        glTexCoord2d(30,0);glVertex3f (-19.3f,2.2f,21);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-19.3f,-0.5f,-3.15f);//esquina inferior izquierda
        glTexCoord2d(0,30);glVertex3f (-19.5f,-0.5f,-3.15f);//esquina inferior derecha
        glTexCoord2d(30,30);glVertex3f (-19.5f,2.2f,-3.15f);//esquina superior derecha
        glTexCoord2d(30,0);glVertex3f (-19.3f,2.2f,-3.15f);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-19.3f,-0.5f,21.15f);//esquina inferior izquierda
        glTexCoord2d(0,30);glVertex3f (-19.5f,-0.5f,21.15f);//esquina inferior derecha
        glTexCoord2d(30,30);glVertex3f (-19.5f,2.2f,21.15f);//esquina superior derecha
        glTexCoord2d(30,0);glVertex3f (-19.3f,2.2f,21.15f);//esquina superior izquierda
        
        
        glEnd();
        
        }
        
        public void drawArcos(){
            
        glBegin (GL_QUADS);
        
        //arco 1
        glTexCoord2d(0,0);glVertex3f (-8.2f,-0.5f,-2.8f);//esquina inferior izquierda
        glTexCoord2d(0,30);glVertex3f (-8.2f,-0.5f,-2.95f);//esquina inferior derecha
        glTexCoord2d(30,30);glVertex3f (-8.2f,1.6f,-2.95f);//esquina superior derecha
        glTexCoord2d(30,0);glVertex3f (-8.2f,1.6f,-2.8f);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-8.3f,-0.5f,-2.8f);//esquina inferior izquierda
        glTexCoord2d(0,30);glVertex3f (-8.3f,-0.5f,-2.95f);//esquina inferior derecha
        glTexCoord2d(30,30);glVertex3f (-8.3f,1.6f,-2.95f);//esquina superior derecha
        glTexCoord2d(30,0);glVertex3f (-8.3f,1.6f,-2.8f);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-12f,-0.5f,-2.8f);//esquina inferior izquierda
        glTexCoord2d(0,30);glVertex3f (-12f,-0.5f,-2.95f);//esquina inferior derecha
        glTexCoord2d(30,30);glVertex3f (-12f,1.6f,-2.95f);//esquina superior derecha
        glTexCoord2d(30,0);glVertex3f (-12f,1.6f,-2.8f);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-12.1f,-0.5f,-2.8f);//esquina inferior izquierda
        glTexCoord2d(0,30);glVertex3f (-12.1f,-0.5f,-2.95f);//esquina inferior derecha
        glTexCoord2d(30,30);glVertex3f (-12.1f,1.6f,-2.95f);//esquina superior derecha
        glTexCoord2d(30,0);glVertex3f (-12.1f,1.6f,-2.8f);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-8.2f,-0.5f,-2.8f);//esquina inferior izquierda
        glTexCoord2d(0,30);glVertex3f (-8.3f,-0.5f,-2.8f);//esquina inferior derecha
        glTexCoord2d(30,30);glVertex3f (-8.3f,1.6f,-2.8f);//esquina superior derecha
        glTexCoord2d(30,0);glVertex3f (-8.2f,1.6f,-2.8f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-12f,-0.5f,-2.8f);//esquina inferior izquierda
        glTexCoord2d(0,30);glVertex3f (-12.1f,-0.5f,-2.8f);//esquina inferior derecha
        glTexCoord2d(30,30);glVertex3f (-12.1f,1.6f,-2.8f);//esquina superior derecha
        glTexCoord2d(30,0);glVertex3f (-12f,1.6f,-2.8f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-8.2f,1.5f,-2.8f);//esquina inferior izquierda
        glTexCoord2d(0,30);glVertex3f (-12.1f,1.5f,-2.8f);//esquina inferior derecha
        glTexCoord2d(30,30);glVertex3f (-12.1f,1.6f,-2.8f);//esquina superior derecha
        glTexCoord2d(30,0);glVertex3f (-8.2f,1.6f,-2.8f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-8.2f,1.6f,-2.9f);//esquina inferior izquierda
        glTexCoord2d(0,30);glVertex3f (-12.1f,1.6f,-2.9f);//esquina inferior derecha
        glTexCoord2d(30,30);glVertex3f (-12.1f,1.6f,-2.8f);//esquina superior derecha
        glTexCoord2d(30,0);glVertex3f (-8.2f,1.6f,-2.8f);//esquina superior izquierda
        
        //arco 2
        glTexCoord2d(0,0);glVertex3f (-8.2f,-0.5f,20.8f);//esquina inferior izquierda
        glTexCoord2d(0,30);glVertex3f (-8.2f,-0.5f,20.95f);//esquina inferior derecha
        glTexCoord2d(30,30);glVertex3f (-8.2f,1.6f,20.95f);//esquina superior derecha
        glTexCoord2d(30,0);glVertex3f (-8.2f,1.6f,20.8f);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-8.3f,-0.5f,20.8f);//esquina inferior izquierda
        glTexCoord2d(0,30);glVertex3f (-8.3f,-0.5f,20.95f);//esquina inferior derecha
        glTexCoord2d(30,30);glVertex3f (-8.3f,1.6f,20.95f);//esquina superior derecha
        glTexCoord2d(30,0);glVertex3f (-8.3f,1.6f,20.8f);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-12f,-0.5f,20.8f);//esquina inferior izquierda
        glTexCoord2d(0,30);glVertex3f (-12f,-0.5f,20.95f);//esquina inferior derecha
        glTexCoord2d(30,30);glVertex3f (-12f,1.6f,20.95f);//esquina superior derecha
        glTexCoord2d(30,0);glVertex3f (-12f,1.6f,20.8f);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-12.1f,-0.5f,20.8f);//esquina inferior izquierda
        glTexCoord2d(0,30);glVertex3f (-12.1f,-0.5f,20.95f);//esquina inferior derecha
        glTexCoord2d(30,30);glVertex3f (-12.1f,1.6f,20.95f);//esquina superior derecha
        glTexCoord2d(30,0);glVertex3f (-12.1f,1.6f,20.8f);//esquina superior izquierda 
        
        glTexCoord2d(0,0);glVertex3f (-8.2f,-0.5f,20.8f);//esquina inferior izquierda
        glTexCoord2d(0,30);glVertex3f (-8.3f,-0.5f,20.8f);//esquina inferior derecha
        glTexCoord2d(30,30);glVertex3f (-8.3f,1.6f,20.8f);//esquina superior derecha
        glTexCoord2d(30,0);glVertex3f (-8.2f,1.6f,20.8f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-12f,-0.5f,20.8f);//esquina inferior izquierda
        glTexCoord2d(0,30);glVertex3f (-12.1f,-0.5f,20.8f);//esquina inferior derecha
        glTexCoord2d(30,30);glVertex3f (-12.1f,1.6f,20.8f);//esquina superior derecha
        glTexCoord2d(30,0);glVertex3f (-12f,1.6f,20.8f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-8.2f,1.5f,20.8f);//esquina inferior izquierda
        glTexCoord2d(0,30);glVertex3f (-12.1f,1.5f,20.8f);//esquina inferior derecha
        glTexCoord2d(30,30);glVertex3f (-12.1f,1.6f,20.8f);//esquina superior derecha
        glTexCoord2d(30,0);glVertex3f (-8.2f,1.6f,20.8f);//esquina superior izquierda
        
        glTexCoord2d(0,0);glVertex3f (-8.2f,1.6f,20.9f);//esquina inferior izquierda
        glTexCoord2d(0,30);glVertex3f (-12.1f,1.6f,20.9f);//esquina inferior derecha
        glTexCoord2d(30,30);glVertex3f (-12.1f,1.6f,20.8f);//esquina superior derecha
        glTexCoord2d(30,0);glVertex3f (-8.2f,1.6f,20.8f);//esquina superior izquierda
        
        glEnd();
        }
        
}
